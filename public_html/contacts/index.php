<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("not_show_wrapper_class", "Y");
$APPLICATION->SetTitle("Контакты");
\ZLabs\JSCore::addBlocks(['contacts']);
?>
<div class="contacts" itemscope itemtype="http://schema.org/Organization">
    <span itemprop="name" style="display: none;">МУП «Электросервис»</span>
	<div class="container">
		<div class="contacts__blocks clearfix">
			<div class="contacts__block">
				<div class="icon icon_calling contacts-block__icon">
				</div>
				<div class="contacts-block__title">
					 Приемная (многоканальный)
				</div>
				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/local/included_areas/contacts/block1.php"
	)
);?>
			</div>
			<div class="contacts__block">
				<div class="icon icon_envelope contacts-block__icon">
				</div>
				<div class="contacts-block__title">
					 Электронная почта
				</div>
				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/local/included_areas/contacts/block2.php"
	)
);?>
			</div>
		</div>
	</div>
	<div class="contacts-map__container">
		<div class="contacts-map__header">
			 Где мы находимся
		</div>
		 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/local/included_areas/contacts/address.php"
	)
);?>
		<div class="contacts__map">
			<div class="contacts-map__canvas" id="YMap" style="width: 100%; height: 500px;">
			</div>
			<div class="contacts-map__preloader">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="contacts-download__container">
 <a href="/content/реквизиты.txt" download="" class="link contacts__download-link" title="Скачать реквизиты"> <span class="contacts__download-icon icon icon_arrow-down"></span> <span class="contacts__download-underlay">Скачать реквизиты</span> </a>
		</div>
		<div class="contacts__tels-and-form clearfix">
			<div class="contacts__tels">
				<div class="contacts-tels__header">
					 Телефоны
				</div>
				<div class="contacts-tels__container">
					<div class="contacts-tels__tel icon icon_calling contacts-tels__icon">
						<div class="contacts-tels-tel__title">
							 ПТО
						</div>
						 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/local/included_areas/contacts/tel1.php"
	)
);?>
					</div>
					<div class="contacts-tels__tel icon icon_calling contacts-tels__icon">
						<div class="contacts-tels-tel__title">
							 Бухгалтерия
						</div>
						 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/local/included_areas/contacts/tel1.php"
	)
);?>
					</div>
					<div class="contacts-tels__tel icon icon_calling contacts-tels__icon">
						<div class="contacts-tels-tel__title">
							 Юрисконсульт
						</div>
						 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/local/included_areas/contacts/tel3.php"
	)
);?>
					</div>
					<div class="contacts-tels__tel icon icon_calling contacts-tels__icon">
						<div class="contacts-tels-tel__title">
							 Диспетчерская
						</div>
						 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/local/included_areas/contacts/tel4.php"
	)
);?>
					</div>
					<div class="contacts-tels__tel icon icon_fax contacts-tels__icon">
						<div class="contacts-tels-tel__title">
							 Факс
						</div>
						 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/local/included_areas/contacts/tel5.php"
	)
);?>
					</div>
				</div>
			</div>
			<div class="contacts__form">
                <?$APPLICATION->IncludeComponent(
	"zlabs:feedbackform.form", 
	"feedback.form.contact", 
	array(
		"COMPONENT_TEMPLATE" => "feedback.form.contact",
		"EMAIL_TO" => array(
			0 => "mup@es-sakh.su",
			1 => "",
		),
		"EVENT_MESSAGE_ID" => array(
			0 => "8",
		),
		"FIELD_0_CODE" => "NAME",
		"FIELD_0_MASK" => "SIMPLE",
		"FIELD_0_NOTE" => "",
		"FIELD_0_PLACEHOLDER" => "Введите ФИО",
		"FIELD_0_REQUIRE" => "Y",
		"FIELD_0_TITLE" => "Имя отправителя",
		"FIELD_0_TYPE" => "TEXT",
		"FIELD_1_CODE" => "EMAIL",
		"FIELD_1_MASK" => "EMAIL",
		"FIELD_1_NOTE" => "",
		"FIELD_1_PLACEHOLDER" => "Ваш email",
		"FIELD_1_REQUIRE" => "Y",
		"FIELD_1_TITLE" => "Почта отправителя",
		"FIELD_1_TYPE" => "TEXT",
		"FIELD_2_CODE" => "SUBJECT",
		"FIELD_2_MASK" => "SIMPLE",
		"FIELD_2_NOTE" => "",
		"FIELD_2_PLACEHOLDER" => "Тема письма",
		"FIELD_2_REQUIRE" => "N",
		"FIELD_2_TITLE" => "Тема письма",
		"FIELD_2_TYPE" => "TEXT",
		"FIELD_3_CODE" => "MESSAGE",
		"FIELD_3_HEIGHT" => "DEFAULT",
		"FIELD_3_MASK" => "SIMPLE",
		"FIELD_3_NOTE" => "",
		"FIELD_3_PLACEHOLDER" => "Ваше сообщение",
		"FIELD_3_REQUIRE" => "Y",
		"FIELD_3_TITLE" => "Сообщение",
		"FIELD_3_TYPE" => "TEXTAREA",
		"FOOTNOTE" => "",
		"GOALS" => array(
		),
		"ID" => "letter",
		"NAME" => "Письмо со страницы Контакты",
		"NUM_FIELDS" => "4",
		"POPUP_FORM" => "N",
		"SUBMIT" => "Отправить сообщение",
		"SUB_TITLE" => "",
		"SUCCESS_MESSAGE" => "В близжайшее время с вами свяжется наш менеджер",
		"SUCCESS_MESSAGE_TITLE" => "Ваша заявка принята",
		"TITLE" => "Написать письмо",
		"USER_CONSENT" => "Y",
		"USER_CONSENT_ID" => "1",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N"
	),
	false
);?>

			</div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>