<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

\ZLabs\JSCore::addBlocks(['error']);

$APPLICATION->SetTitle("Страница не найдена");?>

    <div class="error">
        <div class="error__404">404</div>
        <h3 class="error__title">Запрашиваемая вами<br>страница — не существует</h3>
        <div class="btn btn_main error__btn"><a href="/" class="link btn__link error__btn-link" title="На главную">На главную</a></div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>