<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Технологическое присоединение");
\ZLabs\JSCore::addBlocks(['connection']);
?>

    <div class="connection">
        <div class="container">
            <div class="connection__list connection__counter">
                <h2 class="connection__counter-item connection__counter-item_red h2 connection__header">Подача заявки и<br>заключение договора </h2>
                <div class="connection__item">
                    <table class="connection__table">
                        <tbody>
                        <tr>
                            <th class="connection__head-cell">Ваши действия</th>
                            <th class="connection__head-cell">Наши действия</th>
                        </tr>
                        <tr>
                            <td rowspan="2" class="connection__cell connection__cell_active">
                                Подать заявку на подключение<br>одним из способов:<br>
                                <span class="connection__link connection__link_underline">On-line</span>,
                                <span class="connection__link connection__link_dashed">Телефон</span>,
                                <span class="connection__link connection__link_dashed">Лично</span>,
                                <span class="connection__link connection__link_dashed">Почта</span>
                            </td>
                            <td class="connection__cell">Уведомить о недостающих сведениях<br>и документах (при необходимости)</td>
                        </tr>
                        <tr>
                            <td class="connection__cell">Подготовить проект договора <br>и технических условий</td>
                        </tr>
                        <tr>
                            <td class="connection__cell connection__cell_active">Получить проект Договора ТП и проект Договора<br>энергоснабжения в течение 10 дней</td>
                            <td class="connection__cell">Уведомить заявителя о готовности проекта<br>Договора в течение 10 дней</td>
                        </tr>
                        <tr>
                            <td class="connection__cell connection__cell_active">Подписать Договор ТП и проект<br>Договора энергоснабжения</td>
                            <td class="connection__cell">Зарегистрировать в установленном порядке<br>подписанный заявителем Договор</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <h2 class="connection__counter-item connection__counter-item_yellow connection__header">Выполнение технических условий<br>и фактическое присоединение</h2>
                <div class="connection__item">
                    <table class="connection__table">
                        <tbody>
                        <tr>
                            <th class="connection__head-cell">Ваши действия</th>
                            <th class="connection__head-cell">Наши действия</th>
                        </tr>
                        <tr>
                            <td class="connection__cell connection__cell_active">Выполнить свою часть мероприятий<br>по техническим условиям.</td>
                            <td class="connection__cell">Выполнить свою часть мероприятий<br>по техническим условиям</td>
                        </tr>
                        <tr>
                            <td class="connection__cell connection__cell_active">Оплатить подключение</td>
                            <td class="connection__cell ">Подготовить пакет документов о технологическом<br>присоединении и направить копии в сбытовую компанию</td>
                        </tr>
                        <tr>
                            <td class="connection__cell connection__cell_active">Уведомить сетевую организацию о необходимости<br>осмотра (обследования) присоединяемых<br>электроустановок</td>
                            <td class="connection__cell">Принять участие в осмотре (обследовании)<br>присоединяемых электроустановок</td>
                        </tr>
                        <tr>
                            <td class="connection__cell connection__cell_active">Предоставить доступ к присоединяемой<br>электроустановке для осуществления<br>осмотра (обследования)</td>
                            <td class="connection__cell">Осуществить фактическое присоединение объектов<br>заявителя к электрическим сетям</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <h2 class="connection__counter-item connection__counter-item_green connection__header">Получение актов о технологическом<br>присоединении и заключение договора<br>энергоснабжения</h2>
                <div class="connection__item">
                    <table class="connection__table">
                        <tbody>
                        <tr>
                            <th class="connection__head-cell">Ваши действия</th>
                            <th class="connection__head-cell">Наши действия</th>
                        </tr>
                        <tr>
                            <td class="connection__cell connection__cell_active">Получить и подписать документы<br>о технологическом присоединении</td>
                            <td class="connection__cell">Выдать Заявителю документы о технологическом<br>присоединении, передать подписанные акты<br>в сбытовую компанию</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>