<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заявка");
?>

<? $APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"sidebar.menu", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "12000",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "sidebar",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "sidebar.menu",
		"SIDEBAR_TALL" => "TALL"
	),
	false
); ?>

<?$APPLICATION->IncludeComponent(
	"zlabs:lk.order",
	"new-template",
	Array(
		"COMPONENT_TEMPLATE" => "new-template"
	)
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>