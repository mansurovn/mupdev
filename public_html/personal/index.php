<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Персональный раздел");
ZLabs\JSCore::addBlocks(['sidebar']);
?>
<? $APPLICATION->IncludeComponent("bitrix:menu", "sidebar.menu", Array(
    "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
    "CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
    "DELAY" => "N",    // Откладывать выполнение шаблона меню
    "MAX_LEVEL" => "1",    // Уровень вложенности меню
    "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
    "MENU_CACHE_TIME" => "12000",    // Время кеширования (сек.)
    "MENU_CACHE_TYPE" => "A",    // Тип кеширования
    "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
    "ROOT_MENU_TYPE" => "sidebar",    // Тип меню для первого уровня
    "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
    "COMPONENT_TEMPLATE" => "top.menu"
),
    false
); ?>
    <div class="text text-wrap">В личном кабинете вы можете <a href="/personal/make-order/" title="подать заявку">подать
            заявку</a> на технологическое присоединение, а так же следить за статусом своей заявки в разделе <a
                href="/personal/my-order/" title="подать заявку">мои заявки</a>.
    </div><br>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>