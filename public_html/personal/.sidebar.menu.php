<?
$aMenuLinks = Array(
	Array(
		"Подать заявку", 
		"/personal/make-order/", 
		Array(), 
		Array("ICON"=>"icon_list"), 
		"" 
	),
	Array(
		"Мои заявки", 
		"/personal/my-order/", 
		Array(), 
		Array("ICON"=>"icon_file"), 
		"" 
	),
	Array(
		"Заявки в работе", 
		"/personal/in-work-order/",
		Array(), 
		Array("ICON"=>"icon_clock"), 
		"" 
	),
	Array(
		"Завершенные заявки", 
		"/personal/complete-order/",
		Array(), 
		Array("ICON"=>"icon_list-checked"), 
		"" 
	),
	Array(
		"Калькулятор", 
		"#calc", 
		Array(), 
		Array("ICON"=>"icon_divide", "CALC"=>"sidebar__link_calc"), 
		"" 
	),
	Array(
		"Настройки", 
		"/personal/settings/", 
		Array(), 
		Array("ICON"=>"icon_gear"), 
		"" 
	),
	Array(
		"Выход", 
		"/personal/?logout=yes", 
		Array(), 
		Array("ICON"=>"icon_door"), 
		"" 
	)
);
?>