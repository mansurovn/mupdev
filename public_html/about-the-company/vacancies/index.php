<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Работа в компании");

\ZLabs\JSCore::addBlocks(['question']);
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"vacancies.list",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "vacancies",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "5",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"DUTIES",1=>"ADDITIONAL",2=>"SALARY",3=>"EXPERIENCE",4=>"CONDITIONS",5=>"DEMANDS",6=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
    <br>
 <?$APPLICATION->IncludeComponent(
	"zlabs:feedbackform.form", 
	"feedback.form", 
	array(
		"EMAIL_TO" => array(
			0 => "es_lawyer@mail.ru",
			1 => "",
		),
		"EVENT_MESSAGE_ID" => array(
			0 => "8",
		),
		"FIELD_0_CODE" => "NAME",
		"FIELD_0_MASK" => "SIMPLE",
		"FIELD_0_NOTE" => "",
		"FIELD_0_PLACEHOLDER" => "Введите ФИО",
		"FIELD_0_REQUIRE" => "Y",
		"FIELD_0_TITLE" => "Имя отправителя",
		"FIELD_0_TYPE" => "TEXT",
		"FIELD_1_CODE" => "TEL",
		"FIELD_1_MASK" => "PHONE",
		"FIELD_1_NOTE" => "",
		"FIELD_1_PLACEHOLDER" => "Ваш телефон",
		"FIELD_1_REQUIRE" => "Y",
		"FIELD_1_TITLE" => "Телефон",
		"FIELD_1_TYPE" => "TEXT",
		"FIELD_2_CODE" => "EMAIL",
		"FIELD_2_MASK" => "EMAIL",
		"FIELD_2_NOTE" => "",
		"FIELD_2_PLACEHOLDER" => "Электронная почта",
		"FIELD_2_REQUIRE" => "Y",
		"FIELD_2_TITLE" => "Электронная почта",
		"FIELD_2_TYPE" => "TEXT",
		"FIELD_3_CODE" => "RESUME",
		"FIELD_3_MASK" => "SIMPLE",
		"FIELD_3_MULTIPLE" => "N",
		"FIELD_3_NOTE" => "",
		"FIELD_3_PLACEHOLDER" => "Прикрепить резюме",
		"FIELD_3_REQUIRE" => "N",
		"FIELD_3_TITLE" => "Резюме",
		"FIELD_3_TYPE" => "FILE",
		"FOOTNOTE" => "",
		"GOALS" => array(
		),
		"ID" => "vacancy",
		"LINK_TO_FORM" => "",
		"NAME" => "Отклик на вакансию",
		"NUM_FIELDS" => "4",
		"POPUP_FORM" => "Y",
		"SUBMIT" => "Отправить заявку",
		"SUB_TITLE" => "",
		"SUCCESS_MESSAGE" => "В близжайшее время с вами свяжется наш менеджер",
		"SUCCESS_MESSAGE_TITLE" => "Ваше резюме принято",
		"TITLE" => "Откликнуться
на вакансию",
		"USER_CONSENT" => "Y",
		"USER_CONSENT_ID" => "1",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"COMPONENT_TEMPLATE" => "feedback.form"
	),
	false
);?>
<section class="question clearfix">
<div class="question__left-col">

    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "inc",
            "EDIT_TEMPLATE" => "",
            "PATH" => "/local/included_areas/vacancy/title.php"
        )
    );?>

</div>
<div class="question__right-col">
	<div class="question__title-and-tel">
		<div class="question__title">
			 Отдел кадров
		</div>

        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/local/included_areas/vacancy/tel.php"
            )
        );?>

	</div>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>