<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Закупки");
?><div class="text text-wrap">
	<p>
 <a href="http://es.zandi.tmweb.ru/images/content/polozhenie-o-zakupkah-mup-elektroservice.pdf" rel="nofollow" target="_blank" title="Положение о закупках МУП «Электросервис»">Положение о закупках МУП «Электросервис»</a> 
</p>
<p>
<a href="http://es.zandi.tmweb.ru/images/content/polozhenie-o-zakupkah-oao-elektroservice.pdf" rel="nofollow" target="_blank" title="Положение о закупках ОАО «Электросервис»">Положение о закупках ОАО «Электросервис»</a>
	</p>
 <br>
	<p>
		 В соответствии с Законом N 321-ФЗ внесены изменения в Федеральный закон от 5 апреля 2013 г. N 44-ФЗ "О контрактной системе в сфере закупок товаров, работ, услуг для обеспечения государственных и муниципальных нужд" (далее - Закон N 44-ФЗ) и в Федеральный закон от 18 июля 2011 года N 223-ФЗ "О закупках товаров, работ, услуг отдельными видами юридических лиц" (далее - Закон N 223-ФЗ), с 1 января 2017 года унитарное предприятие является заказчиком, подпадающим под действие Закона N 44-ФЗ.
	</p>
	<p>
		 Таким образом, с 01.01.2017 г. МУП "Электросервис" осуществляет закупки на основании и в соответствии с Законом N 44-ФЗ. Информация о планируемых закупках, а также о планируемых объемах закупок размещается <a href="http://zakupki.gov.ru" rel="nofollow" target="_blank" title="Единая информационная система в сфере закупок">на официальном сайте</a>, определенном Правительством РФ (Постановление Правительства РФ от 23.12.2015 N 1414 "О порядке функционирования единой информационной системы в сфере закупок").
	</p>
	<p>
 <a href="http://zakupki.gov.ru/epz/purchaseplanfz44/purchasePlanStructuredCard/general-info.html?plan-number=201705616000036002" rel="nofollow" target="_blank" title="План закупок на 2017 год">План закупок на 2017 год. </a>
	</p>
	<p>
 <a href="http://zakupki.gov.ru/pgz/public/action/organization/view?source=epz&organizationId=2192423" rel="nofollow" target="_blank" title="Сведения об организации-заказчике">Сведения об организации-заказчике</a>
	</p>
	<p>
 <a href="http://zakupki.gov.ru/epz/order/quicksearch/search.html?searchString=6501238703&morphology=on&pageNumber=1&sortDirection=false&recordsPerPage=_10&showLotsInfoHidden=false&fz44=on&fz223=on&af=on&ca=on&pc=on&pa=on&priceFrom=&priceTo=&currencyId=1&agencyTitle=&agencyCode=&agencyFz94id=&agencyFz223id=&agencyInn=&regions=&publishDateFrom=&publishDateTo=&sortBy=UPDATE_DATE&updateDateFrom=&updateDateTo=" rel="nofollow" target="_blank" title="Сведения о закупках">Сведения о закупках</a>
	</p>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>