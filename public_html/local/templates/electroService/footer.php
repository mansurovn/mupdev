<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
        </main>
        <footer class="footer">
            <div class="container">
                <div class="footer__contacts clearfix">
                    <div class="footer__contact footer__contact__wide">
                        <div class="footer-contact__title-and-text">

                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/included_areas/footer/block1.php"
                                )
                            );?>

                        </div>
                    </div>
                    <div class="footer__contact">
                        <div class="footer-contact__title-and-text">
                            <div class="footer-contact__icon icon icon_envelope"></div>

                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/included_areas/footer/block2.php"
                                )
                            );?>

                        </div>
                    </div>
                    <div class="footer__contact">
                        <div class="footer-contact__title-and-text">
                            <div class="footer-contact__icon icon icon_calling"></div>

                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/included_areas/footer/block3.php"
                                )
                            );?>

                        </div>
                    </div>
                    <div class="footer__contact">
                        <div class="footer-contact__title-and-text">
                            <div class="footer-contact__icon icon icon_calling"></div>

                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/included_areas/footer/block4.php"
                                )
                            );?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="footer__signs">
                <div class="container">
                    <div class="footer__copyright">
                        ОАО "Электросервис", МУП "Электросервис" © 2014. 693004, Сахалинская область, г. Южно-Сахалинск, ул. Ленина, 378-А<br>
                        При полном или частичном использовании материалов ссылка на es-sakh.su (для сайтов - интерактивная) обязательна.
                    </div>
                    <div class="footer__dev-sign">
                        <div class="footer-dev-sign__title">Разработка сайта</div>
                        <a href="z-labs.ru" rel="nofollow" target="_blank" class="link footer-dev-sign__link" title="Студия «Z-Labs»">Студия «Z-Labs»</a>
                    </div>
                </div>
            </div>
        </footer>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/local/included_areas/footer/web-masters.php"
    )
);?>
    </body>
</html>