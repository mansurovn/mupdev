<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}


$needObtainMustache = !empty($arResult);
$needObtainSearchBar = true;

$arMustache = [];
$parent = 0;

if ($needObtainMustache) {
    foreach ($arResult as $itemKey => $arItem) {
        $arMustacheItem = array(
            'id' => $arItem["ITEM_INDEX"],
            'name' => $arItem['TEXT'],
            'link' => $arItem['LINK'],
            'activeItem' => $arItem["SELECTED"] ? ($arItem['DEPTH_LEVEL'] == 1 ? 'nav__link_active' : 'nav-sub__link_active') : '',
            'hasChild' => $arItem['IS_PARENT'],
            'arrow' => $arItem['IS_PARENT'] ? "icon icon_arrow-angle-down nav__icon" : ""
        );

        if ($arItem["DEPTH_LEVEL"] == 1) {
            $parent = $arMustacheItem['id'];
            $arMustache['items'][] = $arMustacheItem;
        } else {
            $arMustache['items'][$parent]['hasChild'] = true;
            $arMustache['items'][$parent]['child'][] = $arMustacheItem;
        }
    }

    $arResult = [];
    $arResult['MUSTACHE'] = $arMustache;
    unset($arMustache);
}

/*if ($needObtainSearchBar) {
    $arResult['MUSTACHE']['search'] = '<li class="nav__item nav__item_search">
    <div class="nav__search">
        <span class="icon icon_magnifier nav-search__icon"></span>
        <div class="nav-search-group">
            <?$APPLICATION->IncludeComponent(
                "bitrix:search.suggest.input",
                "search.nav",
                Array(
                    "DROPDOWN_SIZE" => "10",
                    "INPUT_SIZE" => "40",
                    "NAME" => "q",
                    "VALUE" => ""
                )
            );?>
        </div>
    </div>
</li>';
}*/