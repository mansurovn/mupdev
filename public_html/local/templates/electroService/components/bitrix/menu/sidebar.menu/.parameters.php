<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "SIDEBAR_TALL" => array(
        "NAME" => "Отступ сверху",
        "TYPE" => "LIST",
        "VALUES" => array(
            "DEFAULT" => "Обычный",
            "MIDDLE" => "Средний",
            "TALL" => "Большой",
        ),
    ),
);