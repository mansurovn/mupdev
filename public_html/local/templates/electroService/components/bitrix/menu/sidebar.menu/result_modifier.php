<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}


$needObtainMustache = !empty($arResult);
$needObtainSearchBar = true;

$arMustache = [];
$parent = 0;

if ($needObtainMustache) {
    foreach ($arResult as $itemKey => $arItem) {
        $arMustacheItem = array(
            'id' => $arItem["ITEM_INDEX"],
            'name' => $arItem['TEXT'],
            'link' => $arItem['LINK'],
            'activeItem' => $arItem["SELECTED"] ?  'sidebar__link_active' :  '',
            'hasChild' => $arItem['IS_PARENT'],
            'icon' => $arItem['PARAMS']['ICON'],
            'calc' => $arItem['PARAMS']['CALC']
        );

        if ($arItem["DEPTH_LEVEL"] == 1) {
            $parent = $arMustacheItem['id'];
            $arMustache['items'][] = $arMustacheItem;
        } else {
            $arMustache['items'][$parent]['hasChild'] = true;
            $arMustache['items'][$parent]['child'][] = $arMustacheItem;
        }
    }

    $arResult = [];
    $arResult['MUSTACHE'] = $arMustache;
    switch ($arParams['SIDEBAR_TALL']) {
        case "TALL":
            $arResult['MUSTACHE']['addClass'] = 'sidebar_tall';
            break;
        case "MIDDLE":
            $arResult['MUSTACHE']['addClass'] = 'sidebar_mid';
            break;
    }
    unset($arMustache);
}