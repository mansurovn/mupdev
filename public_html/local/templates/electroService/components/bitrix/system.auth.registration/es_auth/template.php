<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="bx-auth text text-wrap">
<?
ShowMessage($arParams["~AUTH_RESULT"]);
?>
<?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) &&  $arParams["AUTH_RESULT"]["TYPE"] === "OK"):?>
    <p><?echo GetMessage("AUTH_EMAIL_SENT")?></p>
<?else:?>

<?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):?>
	<p><?echo GetMessage("AUTH_EMAIL_WILL_BE_SENT")?></p>
<?endif?>
<noindex>
    <div class="feedback-form feedback-form_auth">
        <form method="post" action="<?=$arResult["AUTH_URL"]?>" class="feedback-form" name="bform" enctype="multipart/form-data">
        <?
        if (strlen($arResult["BACKURL"]) > 0)
        {
        ?>
            <input type="hidden" class="feedback-form__control feedback-form__control__hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
        <?
        }
        ?>
            <input type="hidden" class="feedback-form__control feedback-form__control__hidden"  name="AUTH_FORM" value="Y" />
            <input type="hidden" class="feedback-form__control feedback-form__control__hidden"  name="TYPE" value="REGISTRATION" />

            <div class="feedback-form__group">
                <h3><?=GetMessage("AUTH_REGISTER")?></h3>
            </div>
            <div class="feedback-form__group">
                <input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" class="feedback-form__control feedback-form__control_type_text" placeholder="Введите логин"/>
            </div>
            <div class="feedback-form__group">
                <input type="password" name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" class="feedback-form__control feedback-form__control_type_text" autocomplete="off" placeholder="Введите пароль"/>
            </div>
            <div class="feedback-form__group">
                <input type="password" name="USER_CONFIRM_PASSWORD" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" class="feedback-form__control feedback-form__control_type_text" autocomplete="off" placeholder="Подтвердите пароль"/></td>
            </div>
            <div class="feedback-form__group">
                <input type="text" name="USER_EMAIL" class="feedback-form__control feedback-form__control_type_text" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>" placeholder="Введите e-mail"/>
            </div>
            <div class="feedback-form__user-consent">
                <?php
                $APPLICATION->IncludeComponent('bitrix:main.userconsent.request', 'simple', array(
                    'ID' => 1,    // Соглашение
                    'IS_CHECKED' => "Y",    // Галка согласия проставлена по умолчанию
                    'AUTO_SAVE' => 'Y',    // Сохранять автоматически факт согласия
                    'IS_LOADED' => "N",    // Загружать текст соглашения сразу
                ),
                    false
                );
                ?>
            </div>
            <?php
                $disabledSubmit = ($arParams['USER_CONSENT'] === 'Y' && $arParams['USER_CONSENT_IS_CHECKED'] === 'N')
                    ? 'disabled="disabled"' : ''
            ?>

            <div class="feedback-form__submit-container">
                <button class="feedback-form__submit" <?=$disabledSubmit;?> type="submit" name="submit"><span class="btn__link">Зарегистироваться</span></button>
                <div class="feedback-form__footnote"></div>
            </div>

            <div class="feedback-form__group">
                <a href="<?=$arResult["AUTH_AUTH_URL"]?>" class="link feedback-form__form-link" rel="nofollow"><?=GetMessage("AUTH_AUTH")?></a>
            </div>
        </form>
    </div>
</noindex>
<?endif?>
</div>