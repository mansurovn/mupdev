<?php

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $componentPath
 * @var CBitrixComponent $component
 */

$this->setFrameMode(true);

if (!$arResult['NavShowAlways']) {
    if ($arResult['NavRecordCount'] == 0 || ($arResult['NavPageCount'] == 1 && $arResult['NavShowAll'] == false)) {
        return;
    }
}

$strNavQueryString = ($arResult['NavQueryString'] != "" ? $arResult['NavQueryString'] . '&amp;' : '');

if ($arResult['bDescPageNumbering'] === true) :
    if ($arResult['NavPageNomer'] > 1) : ?>
        <button class="infinity btn btn_linear category-panel-footer__btn category-panel-footer__btn-refresh">
            <a href="<?= $arResult['sUrlPath'] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult['NavNum'] ?>=<?=
            ($arResult['NavPageNomer'] - 1) ?>"
               class="link infinity__link btn__link category-panel-footer__btn-icon-refresh icon icon_rotate"
               data-load-text="<?= Loc::getMessage('INFINITY_LOADING_TEXT') ?>"
               id="infinity-next-page">
                <span class="infinity__link-text"><?= Loc::getMessage('INFINITY_BUTTON_TEXT') ?></span>
            </a>
        </button>
        <?php
    endif;
else :
    if ($arResult['NavPageNomer'] < $arResult['NavPageCount']) : ?>
        <button class="infinity btn btn_linear category-panel-footer__btn category-panel-footer__btn-refresh">
            <a href="<?= $arResult['sUrlPath'] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult['NavNum'] ?>=<?=
            ($arResult['NavPageNomer'] + 1) ?>"
               class="link infinity__link btn__link category-panel-footer__btn-icon-refresh icon icon_rotate"
               data-load-text="<?= Loc::getMessage('INFINITY_LOADING_TEXT') ?>"
               id="infinity-next-page">
                <span class="infinity__link-text"><?= Loc::getMessage('INFINITY_BUTTON_TEXT') ?></span>
            </a>
        </button>
        <?php
    endif;
endif;
