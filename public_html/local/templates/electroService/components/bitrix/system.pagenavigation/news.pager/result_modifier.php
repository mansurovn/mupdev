<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arResult['MUSTACHE'] = [];
$arPages = [];
$needObtainMustache = ($arResult['NavRecordCount'] > $arResult['NavPageSize']);
//$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&" : "");

if ($needObtainMustache) {
    for ($i=0; $i<$arResult['NavPageCount']; $i++) {
        $arPages[] = array(
            "link" => $arResult["sUrlPath"]."?"."PAGEN_".$arResult["NavNum"]."=".($i+1),
            "nom" => $i+1,
            "isDest" => ($i+1 == $arResult['NavPageNomer']),
            "isNotDest" => ($i+1 != $arResult['NavPageNomer'])
        );
    }

    $arMustacheItem = array(
        "linkPrev" => $arResult["sUrlPath"]."?"."PAGEN_".$arResult["NavNum"]."=".($arResult['NavPageNomer']-1),
        "isBegin" => ($arResult['NavPageNomer'] == 1),
        "isNotBegin" => ($arResult['NavPageNomer'] != 1),
        "isEnd" => ($arResult['NavPageNomer'] == $arResult['NavPageCount']),
        "isNotEnd" => ($arResult['NavPageNomer'] != $arResult['NavPageCount']),
        "linkNext" => $arResult["sUrlPath"]."?"."PAGEN_".$arResult["NavNum"]."=".($arResult['NavPageNomer']+1),
        "pages" => $arPages,
    );

    $arResult['MUSTACHE']['pager'] = $arMustacheItem;
    unset ($arPages, $arMustacheItem);
}
