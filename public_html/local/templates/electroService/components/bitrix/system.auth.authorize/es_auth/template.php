<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CJSCore::Init();
?>

<div class="bx-auth text text-wrap">
    <div class="feedback-form__info">
        <?
        ShowMessage($arParams["~AUTH_RESULT"]);
        ShowMessage($arResult['ERROR_MESSAGE']);
        ?>
    </div>

    <div class="feedback-form feedback-form_auth">
        <form name="form_auth" method="post" target="_top" class="feedback-form" action="<?= $arResult["AUTH_URL"] ?>">
            <input type="hidden" class="feedback-form__control feedback-form__control_type_hidden" name="AUTH_FORM"
                   value="Y"/>
            <input type="hidden" class="feedback-form__control feedback-form__control_type_hidden" name="TYPE"
                   value="AUTH"/>
            <? if (strlen($arResult["BACKURL"]) > 0) : ?>
                <input type="hidden" class="feedback-form__control feedback-form__control_type_hidden" name="backurl"
                       value="<?= $arResult["BACKURL"] ?>"/>
            <? endif ?>
            <? foreach ($arResult["POST"] as $key => $value) : ?>
                <input type="hidden" class="feedback-form__control feedback-form__control_type_hidden"
                       name="<?= $key ?>" value="<?= $value ?>"/>
            <? endforeach ?>
            <div class="feedback-form__group">
                <input class="bx-auth-input feedback-form__control feedback-form__control_type_text" type="text" name="USER_LOGIN"
                       maxlength="255" value="<?= $arResult["LAST_LOGIN"] ?>" placeholder="Введите логин"/>
            </div>
            <div class="feedback-form__group">
                <input class="bx-auth-input feedback-form__control feedback-form__control_type_text" type="password"
                       name="USER_PASSWORD" maxlength="255" autocomplete="off" placeholder="Введите пароль"/>

            </div>
            <? if ($arResult["STORE_PASSWORD"] == "Y") : ?>
                <div class="feedback-form__user-consent">
                    <label for="USER_REMEMBER"
                           class="custom-control custom-control_type_checkbox main-user-consent-request"
                           title="<?= GetMessage("AUTH_REMEMBER_ME") ?>">
                        <input class="custom-control__input" type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER"
                               value="Y" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name"><?= GetMessage("AUTH_REMEMBER_ME") ?></span>
                    </label>
                </div>
            <? endif; ?>
            <div class="feedback-form__submit-container">
                <button class="feedback-form__submit" type="submit" name="Login">
                    <span class="btn__link"><?= GetMessage("AUTH_AUTHORIZE") ?></span>
                </button>
                <div class="feedback-form__footnote"></div>
            </div>
            <div class="feedback-form__group">
                <noindex><a href="<?= $arResult["AUTH_REGISTER_URL"] ?>" class="link feedback-form__form-link"
                            rel="nofollow"><?= GetMessage("AUTH_REGISTER") ?></a></noindex>
                /
                <noindex><a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>" class="link feedback-form__form-link"
                            rel="nofollow"><?= GetMessage("AUTH_FORGOT_PASSWORD_2") ?></a></noindex>
            </div>
        </form>
    </div>

</div>

