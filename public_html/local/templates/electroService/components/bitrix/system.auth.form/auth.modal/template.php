<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init();

?>
<div>
    <div class="feedback-form feedback-form-modal-wrap" id="auth">
        <div class="auth" >

            <div class="feedback-form__title">
                <div class="h2 feedback-form__title-value">Авторизация пользователя</div>
            </div>
            <?
            if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']) {
                ShowMessage($arResult['ERROR_MESSAGE']);
            }
            ?>
            <form name="system_auth_form<?=$arResult["RND"]?> feedback-form" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
                <?if ($arResult["BACKURL"] <> '') :?>
                    <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                <?endif?>
                <?foreach ($arResult["POST"] as $key => $value) :?>
                    <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
                <?endforeach?>
                <input type="hidden" name="AUTH_FORM" value="Y" />
                <input type="hidden" name="TYPE" value="AUTH" />
                <div class="feedback-form__group">
                    <input class="feedback-form__control feedback-form__control_type_text" placeholder="Введите логин" name="USER_LOGIN" value="">
                    <script>
                        BX.ready(function() {
                            var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
                            if (loginCookie)
                            {
                                var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
                                var loginInput = form.elements["USER_LOGIN"];
                                loginInput.value = loginCookie;
                            }
                        });
                    </script>
                </div>
                <div class="feedback-form__group">
                    <input type="password" class="feedback-form__control feedback-form__control_type_password" placeholder="Введите пароль" name="USER_PASSWORD" autocomplete="off">
                </div>
                <?if ($arParams['USE_CAPTCHA'] === "Y") :?>
                    <div class="feedback-from__group feedback-from__group_captcha">
                        <div class="feedback-from__captcha" id="auth"></div>
                    </div>

                <?endif;?>
                <div class="feedback-form__user-consent">
                    <label for="USER_REMEMBER_frm" class="custom-control custom-control_type_checkbox main-user-consent-request" title="Запомнить меня">
                        <input class="custom-control__input" type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Запомнить меня </span>
                    </label>
                </div>
                <div class="feedback-form__submit-container">
                    <button class="feedback-form__submit" type="submit" name="submit"><span class="btn__link">Войти в кабинет</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>
                <div class="auth__footer">
                    <noindex><a href="<?=$arResult["AUTH_REGISTER_URL"]?>" class="link auth-footer__link" title="Регистрация" rel="nofollow">Регистрация</a></noindex> / <noindex><a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="link auth-footer__link" title="Восстановление пароля" rel="nofollow">Восстановление пароля</a></noindex>
                </div>
            </form>
        </div>
    </div>
</div>

