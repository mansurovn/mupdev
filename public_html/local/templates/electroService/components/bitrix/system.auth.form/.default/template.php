<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init();
?>

<div class="bx-system-auth-form">

<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']) {
    ShowMessage($arResult['ERROR_MESSAGE']);
}
?>

<?if ($arResult["FORM_TYPE"] == "login") :?>
<div class="feedback-form">
    <div class="feedback-form__title">
        <div class="h2 feedback-form__title-value">Авторизация пользователя</div>
    </div>
    <form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" class="feedback-form" action="<?=$arResult["AUTH_URL"]?>">
    <?if($arResult["BACKURL"] <> ''):?>
        <input type="hidden" name="backurl" class="feedback-form__control feedback-form__control_type_hidden" value="<?=$arResult["BACKURL"]?>" />
    <?endif?>
    <?foreach ($arResult["POST"] as $key => $value):?>
        <input type="hidden" name="<?=$key?>"  class="feedback-form__control feedback-form__control_type_hidden" value="<?=$value?>" />
    <?endforeach?>
        <input type="hidden" name="AUTH_FORM" class="feedback-form__control feedback-form__control_type_hidden"  value="Y" />
        <input type="hidden" name="TYPE" class="feedback-form__control feedback-form__control_type_hidden"  value="AUTH" />
        <div class="feedback-form__group">
            <?=GetMessage("AUTH_LOGIN")?>:<br />
            <input type="text" class="feedback-form__control feedback-form__control_type_text" name="USER_LOGIN" value="" placeholder="Введите логин"/>
            <script>
                BX.ready(function() {
                    var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
                    if (loginCookie)
                    {
                        var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
                        var loginInput = form.elements["USER_LOGIN"];
                        loginInput.value = loginCookie;
                    }
                });
            </script>
        </div>
        <div class="feedback-form__group">
            <?=GetMessage("AUTH_PASSWORD")?>:<br />
            <input type="password" name="USER_PASSWORD" class="feedback-form__control feedback-form__control_type_text" autocomplete="off" placeholder="Введите пароль"/>
        </div>
        <?if ($arResult["STORE_PASSWORD"] == "Y"):?>
        <div class="feedback-form__user-consent">
            <label for="USER_REMEMBER_frm"  class="custom-control custom-control_type_checkbox main-user-consent-request" title="<?=GetMessage("AUTH_REMEMBER_ME")?>">
                <input class="custom-control__input" type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" checked>
                <span class="custom-control__indicator icon icon_checked"></span>
                <span class="custom-control__name"><?echo GetMessage("AUTH_REMEMBER_SHORT")?></span>
            </label>
        </div>
        <?endif;?>
        <div class="feedback-form__submit-container">
            <button class="feedback-form__submit" type="submit" name="submit"><span class="btn__link">Войти</span></button>
            <div class="feedback-form__footnote"></div>
        </div>
        <div class="feedback-form__group">
            <noindex><a href="<?=$arResult["AUTH_REGISTER_URL"]?>" class="link feedback-form__link" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a></noindex>
            /
            <noindex><a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="link feedback-form__link" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a></noindex>
        </div>
    </form>
</div>

<?
elseif ($arResult["FORM_TYPE"] == "otp") :
?>
<div class="feedback-form">
    <div class="feedback-form__title">
        <div class="h2 feedback-form__title-value">Авторизация пользователя</div>
    </div>
    <form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" class="feedback-form" action="<?=$arResult["AUTH_URL"]?>">
        <?if($arResult["BACKURL"] <> ''):?>
            <input type="hidden" name="backurl" class="feedback-form__control feedback-form__control_type_hidden"  value="<?=$arResult["BACKURL"]?>" />
        <?endif?>
        <input type="hidden" name="AUTH_FORM" class="feedback-form__control feedback-form__control_type_hidden"  value="Y" />
        <input type="hidden" name="TYPE" class="feedback-form__control feedback-form__control_type_hidden"  value="OTP" />
        <div class="feedback-form__group">
            <div class="feedback-form__label"><?echo GetMessage("auth_form_comp_otp")?></div>
            <input type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off" placeholder="Введите логин"/>
        </div>
        <?if ($arResult["REMEMBER_OTP"] == "Y"):?>
            <div class="feedback-form__user-consent">
                <label for="OTP_REMEMBER_frm"  class="custom-control custom-control_type_checkbox main-user-consent-request" title="<?echo GetMessage("auth_form_comp_otp_remember_title")?>">
                    <input class="custom-control__input" type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER" value="Y" checked>
                    <span class="custom-control__indicator icon icon_checked"></span>
                    <span class="custom-control__name"><?echo GetMessage("auth_form_comp_otp_remember")?></span>
                </label>
            </div>
        <?endif?>
        <div class="feedback-form__submit-container">
            <button class="feedback-form__submit" type="submit" name="submit"><span class="btn__link"><?=GetMessage("AUTH_LOGIN_BUTTON")?></span></button>
            <div class="feedback-form__footnote"></div>
        </div>
        <div class="feedback-form__group">
            <a href="<?=$arResult["AUTH_LOGIN_URL"]?>" class="feedback-form__link" rel="nofollow"><?echo GetMessage("auth_form_comp_auth")?></a></noindex>
        </div>
    </form>
</div>
<?
else:
?>

<div class="feedback-form">
    <form action="<?=$arResult["AUTH_URL"]?>">
        <div class="feedback-form__group">
            <div class="feedback-form__label"><?=$arResult["USER_NAME"]?></div>
            <div class="feedback-form__label"><?=$arResult["USER_LOGIN"]?></div>
            <a href="<?=$arResult["PROFILE_URL"]?>" class="feeedback-form__link" title="<?=GetMessage("AUTH_PROFILE")?>"><?=GetMessage("AUTH_PROFILE")?></a><br />
        </div>
        <div class="feedback-form__submit-container">
            <?foreach ($arResult["GET"] as $key => $value):?>
                <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
            <?endforeach?>
            <input type="hidden" name="logout" value="yes" />
            <button class="feedback-form__submit" type="submit" name="logout_butt"><span class="btn__link"><?=GetMessage("AUTH_LOGOUT_BUTTON")?></span></button>
        </div>
    </form>
</div>
<?endif?>
</div>
