<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<div class="bx-auth-profile">

<?ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>
<script type="text/javascript">
<!--
var opened_sections = [<?
$arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
$arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
if (strlen($arResult["opened"]) > 0)
{
	echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
}
else
{
	$arResult["opened"] = "reg";
	echo "'reg'";
}
?>];
//-->

var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
</script>
<form method="post" name="form1" class="feedback-form feedback-form_auth" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" class="feedback-form__control feedback-form__control_type_hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" class="feedback-form__control feedback-form__control_type_hidden" name="ID" value=<?=$arResult["ID"]?> />

    <div class="feedback-form__group">
        <div class="feedback-form__label"><?=GetMessage('EMAIL')?></div>
        <input type="text" class="bx-auth-input feedback-form__control feedback-form__control_type_text" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" />
    </div>

    <div class="feedback-form__group">
        <div class="feedback-form__label"><?=GetMessage('LOGIN')?></div>
        <input type="text" class="bx-auth-input feedback-form__control feedback-form__control_type_text" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
    </div>

    <?if($arResult["arUser"]["EXTERNAL_AUTH_ID"] == ''):?>

        <div class="feedback-form__group">
            <div class="feedback-form__label"><?=GetMessage('NEW_PASSWORD_REQ')?></div>
            <input type="password" class="bx-auth-input feedback-form__control feedback-form__control_type_text" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off"/>
        </div>

        <div class="feedback-form__group">
            <div class="feedback-form__label"><?=GetMessage('NEW_PASSWORD_CONFIRM')?></div>
            <input type="password" class="bx-auth-input feedback-form__control feedback-form__control_type_text" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" />
        </div>

	<?endif;?>
    <div class="feedback-form__submit-container">
        <button type="submit" name="save" class="feedback-form__submit"><span class="btn__link"><?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?></span></button>
    </div>
</form>

</div>