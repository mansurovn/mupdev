<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arResult['MUSTACHE'] = [];
$needObtainMustache = !empty($arResult['ITEMS']);
$needObtainSections = true;

if ($needObtainSections) {
    $arSections = [];
    foreach ($arResult["ITEMS"] as $arItem) {
        $arSections[] = $arItem['IBLOCK_SECTION_ID'];
    }
    $arSections = array_unique($arSections);
    $res = CIBlockSection::GetList(
        ['NAME' => 'DESC'],
        array(
            'IBLOCK_ID' => $arParams["IBLOCK_ID"],
            'ACTIVE' => 'Y',
            'GLOBAL_ACTIVE' => 'Y',
            'SECTION_ACTIVE' => 'Y',
            "ID" => $arSections
        ),
        true,
        ['ID', 'IBLOCK_ID', 'NAME']
    );
    $i=0;
    $arSections = [];
    while ($arSection = $res->Fetch()) {
        $arResult['MUSTACHE']['standards']['year'][] = [
            'id' => $arSection['ID'],
            'name' => $arSection['NAME'],
            'addClass' => $i === 0 ? 'info-select-box__item_active' : ""
        ];

        if ($i === 0) {
            $arResult['MUSTACHE']['current'] = $arSection['NAME'];
        }

        $i++;
    }
}

if ($needObtainMustache) {
    $strElementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
    $strElementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
    $arElementDeleteParams = array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'));

    foreach ($arResult['ITEMS'] as $itemKey => $arItem) {
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction(
            $arItem['ID'],
            $arItem['DELETE_LINK'],
            $strElementDelete,
            $arElementDeleteParams
        );
        $strMainID = $this->GetEditAreaId($arItem['ID']);

        $arMustacheItem = [
            'strId' => $strMainID,
            'name' => $arItem['NAME'],
            'standard' => $arItem['DISPLAY_PROPERTIES']['STANDARTS']['VALUE'],
            'link' => $arItem['DISPLAY_PROPERTIES']['LINK']['FILE_VALUE']['SRC'],
            'date' => $arItem['ACTIVE_FROM'] ? date('d.m.Y', MakeTimeStamp($arItem['ACTIVE_FROM'])) : false,
            'activeClass' => $arItem['IBLOCK_SECTION_ID'] === $arResult['MUSTACHE']['standards']['year'][0]['id'] ? 'info-table__item info-table__item_active info-table__item_in' : 'info-table__item',
        ];
        foreach ($arResult['MUSTACHE']['standards']['year'] as $year) {
            if ($arItem['IBLOCK_SECTION_ID'] === $year['id']) {
                $arMustacheItem['year'] = $year['name'];
            }
        }

        $arResult['MUSTACHE']['standards']['standard'][] = $arMustacheItem;
    }
    $arResult['MUSTACHE']['standards']['title'] = $arParams['TABLE_TITLE'];
}