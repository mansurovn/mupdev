<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arResult['MUSTACHE'] = [];
$needObtainMustache = !empty($arResult['ITEMS']);

if ($needObtainMustache) {
    $strElementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
    $strElementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
    $arElementDeleteParams = array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'));

    $i=1;
    $k=0;
    foreach ($arResult['ITEMS'] as $itemKey => $arItem) {
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction(
            $arItem['ID'],
            $arItem['DELETE_LINK'],
            $strElementDelete,
            $arElementDeleteParams
        );
        $strMainID = $this->GetEditAreaId($arItem['ID']);

        $arMustacheItem = [
            'strId' => $strMainID,
            'name' => $arItem['NAME'],
            'station' => $arItem['DISPLAY_PROPERTIES']['STATION']['VALUE'],
            'date' => $arItem['DISPLAY_PROPERTIES']['DATE']['VALUE'],
            'consumer' => $arItem['DISPLAY_PROPERTIES']['CONSUMER']['VALUE'],
            //'addClass' => $i == count($arResult['ITEMS']) && $i%2 == 1 ? "outage-list__item_borderless" : ""
        ];

        $arResult['MUSTACHE']['outageList']['row'][$k]['outage'][] = $arMustacheItem;
        $i++;
        $i%2 == 1 ? $k++ : $k;
    }

    $arResult['MUSTACHE']['outageList']['navString'] = $arResult['NAV_STRING'];
}