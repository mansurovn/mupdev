<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arResult['MUSTACHE'] = [];
$needObtainMustache = !empty($arResult['ITEMS']);
$needObtainSections = false;

if ($needObtainSections) {
    $arSections = [];
    foreach ($arResult["ITEMS"] as $arItem) {
        $arSections[] = $arItem['IBLOCK_SECTION_ID'];
    }
    $arSections = array_unique($arSections);
    $res = CIBlockSection::GetList(
        ['NAME' => 'DESC'],
        array(
            'IBLOCK_ID' => $arParams["IBLOCK_ID"],
            'ACTIVE' => 'Y',
            'GLOBAL_ACTIVE' => 'Y',
            'SECTION_ACTIVE' => 'Y',
            "ID" => $arSections
        ),
        true,
        ['ID', 'IBLOCK_ID', 'NAME']
    );
    $i=0;
    $arSections = [];
    while ($arSection = $res->Fetch()) {
        $arResult['MUSTACHE']['year'][] = [
            'id' => $arSection['ID'],
            'name' => $arSection['NAME'],
            'addClass' => $i === 0 ? 'info-select-box__item_active' : ""
        ];
        $i++;
    }
}

if ($needObtainMustache) {
    $strElementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
    $strElementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
    $arElementDeleteParams = array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'));

    foreach ($arResult['ITEMS'] as $itemKey => $arItem) {
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction(
            $arItem['ID'],
            $arItem['DELETE_LINK'],
            $strElementDelete,
            $arElementDeleteParams
        );
        $strMainID = $this->GetEditAreaId($arItem['ID']);

        $arMustacheItem = [
            'strId' => $strMainID,
            'name' => $arItem['NAME'],
            'text' => $arItem['PREVIEW_TEXT'],
            'link' => $arItem['DETAIL_PAGE_URL'],
            'eventDate' => date('d.m.Y', MakeTimeStamp($arItem['DISPLAY_PROPERTIES']['EVENT_DATE']['VALUE'])),
            'date' => $arItem['DATE_CREATE'] ? date('d.m.Y', MakeTimeStamp($arItem['DATE_CREATE'])) : false,
        ];

        $arResult['MUSTACHE']['incomes']['income'][] = $arMustacheItem;
    }
    $arResult['MUSTACHE']['incomes']['title'] = $arParams['TABLE_TITLE'];
}