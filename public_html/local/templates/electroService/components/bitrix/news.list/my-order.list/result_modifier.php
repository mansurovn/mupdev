<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arResult['MUSTACHE'] = [];
$needObtainMustache = !empty($arResult['ITEMS']);

if ($needObtainMustache) {
    $strElementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
    $strElementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
    $arElementDeleteParams = array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'));

    foreach ($arResult['ITEMS'] as $itemKey => $arItem) {
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction(
            $arItem['ID'],
            $arItem['DELETE_LINK'],
            $strElementDelete,
            $arElementDeleteParams
        );
        $strMainID = $this->GetEditAreaId($arItem['ID']);

        $arMustacheItem = [
            'strId' => $strMainID,
            'id' => $arItem['ID'],
            'date' => $arItem['DATE_CREATE'] ? date('d.m.Y', MakeTimeStamp($arItem['DATE_CREATE'])) : false,
            'address' => $arItem['DISPLAY_PROPERTIES']['OBJECT_ADDRESS']['VALUE'],
            'status' => $arItem['DISPLAY_PROPERTIES']['ORDER_STATUS']['VALUE'],
        ];

        switch ($arItem['DISPLAY_PROPERTIES']['ORDER_STATUS']['VALUE']) {
            case 'Принята':
                $arMustacheItem['statusClass'] = 'my-order-table__cell_apply';
                break;
            case 'На рассмотрении':
                $arMustacheItem['statusClass'] = 'my-order-table__cell_wait';
                break;
            case 'Отказано':
                $arMustacheItem['statusClass'] = 'my-order-table__cell_denied';
                break;
            case 'Завершено':
                $arMustacheItem['statusClass'] = 'my-order-table__cell_complete';
                break;
        }
        $arResult['MUSTACHE']['orders']['order'][] = $arMustacheItem;
    }
} else {
    $arResult['MUSTACHE']['empty'] = "Заявок не найдено";
}
