<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arResult['MUSTACHE'] = [];
$needObtainMustache = !empty($arResult['ITEMS']);

if ($needObtainMustache) {
    $strElementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
    $strElementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
    $arElementDeleteParams = array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'));

    foreach ($arResult['ITEMS'] as $itemKey => $arItem) {
        $salaryProperty = $arItem['DISPLAY_PROPERTIES']['SALARY']['VALUE'];
        $expProperty = $arItem['DISPLAY_PROPERTIES']['EXPERIENCE']['VALUE'];
        $dutyProperty = $arItem['DISPLAY_PROPERTIES']['DUTIES']['VALUE']['TEXT'];
        $conditionsProperty = $arItem['DISPLAY_PROPERTIES']['CONDITIONS']['VALUE'];
        $demandsProperty = $arItem['DISPLAY_PROPERTIES']['DEMANDS']['VALUE'];
        $addProperty = $arItem['DISPLAY_PROPERTIES']['ADDITIONAL']['VALUE'];

        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction(
            $arItem['ID'],
            $arItem['DELETE_LINK'],
            $strElementDelete,
            $arElementDeleteParams
        );
        $strMainID = $this->GetEditAreaId($arItem['ID']);

        $arMustacheItem = [
            'strId' => $strMainID,
            'name' => $arItem['~NAME'],
            'salary' => $salaryProperty,
            'exp' => $expProperty,
            'coma' => !empty($salaryProperty) && !empty($expProperty) ? ", " : "",
            'isConditions' => !empty($conditionsProperty),
            'isDemands' => !empty($demandsProperty),
            'isAdd' => !empty($addProperty),
            'isHiderSet' => !empty($dutyProperty) || !empty($demandsProperty) || !empty($conditionsProperty) || !empty($addProperty),
            'duties' => $dutyProperty,
            'conditions' => $conditionsProperty,
            'demands' => $demandsProperty,
            'add' => $addProperty,
        ];
        $arResult['MUSTACHE']['vacancies'][] = $arMustacheItem;
    }
}