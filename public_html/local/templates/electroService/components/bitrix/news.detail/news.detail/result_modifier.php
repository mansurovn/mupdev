<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$mustache = new Mustache_Engine(['loader' => new Mustache_Loader_FilesystemLoader(__DIR__)]);

$arResult['MUSTACHE'] = [];
$arResult['MUSTACHE']['has_images'] = false;
$needObtainOgData = true;
$needObtainMustache = true;
$needObtainImages = !empty($arParams['IMG_PROP']);

if ($needObtainImages) {
    $resizeImage = new ZLabs\ResizeImage(840, 99999, BX_RESIZE_IMAGE_PROPORTIONAL);
    $arImages = [];

    $arImageProp = $arResult['DISPLAY_PROPERTIES'][$arParams['IMG_PROP']];

    $arImages = is_array($arImageProp['DISPLAY_VALUE'])
        ? $arImageProp['FILE_VALUE'] : ($arImageProp['FILE_VALUE'] ? [$arImageProp['FILE_VALUE']] : []);

    if (!empty($arImages)) {
        $arResult['MUSTACHE']['has_images'] = true;
        foreach ($arImages as $keyImage => $arImage) {
            $propertiesPictureTitle = $propertiesPictureAlt = $arImage['DESCRIPTION'] ?: $arResult['NAME'];

            $resizeImage->resize($arImage);

            $mustacheImage = [
                'src' => $arImage['SRC'],
                'alt' => $propertiesPictureAlt,
                'title' => $propertiesPictureTitle,
                'width' => $arImage['WIDTH'],
                'height' => $arImage['HEIGHT'],
                'caption' => $arImage['DESCRIPTION']
            ];
            ob_start();
            echo $mustache->render('img', ['image' => $mustacheImage]);
            $mark = '#IMAGE' . ($keyImage + 1) . '#';
            $arResult['DETAIL_TEXT'] = str_replace($mark, ob_get_contents(), $arResult['DETAIL_TEXT']);
            ob_end_clean();
        }
    }
}

if ($needObtainMustache) {
    $strElementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
    $strElementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
    $arElementDeleteParams = array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'));

    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
    $this->AddDeleteAction(
        $arItem['ID'],
        $arItem['DELETE_LINK'],
        $strElementDelete,
        $arElementDeleteParams
    );
    $strMainID = $this->GetEditAreaId($arItem['ID']);

    $arMustacheItem = [
        'strId' => $strMainID,
        'name' => $arResult['NAME'],
        'text' => $arResult['DETAIL_TEXT'],
        'sectionLink' => $arResult['LIST_PAGE_URL'],
        'date' => date('d.m.Y', MakeTimeStamp($arResult['ACTIVE_FROM']))
    ];
    $arResult['MUSTACHE']['article'][] = $arMustacheItem;
}