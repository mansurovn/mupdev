<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */

$config = \Bitrix\Main\Web\Json::encode($arResult['CONFIG']);
?>
<label class="custom-control custom-control_type_checkbox main-user-consent-request" data-bx-user-consent="<?= htmlspecialcharsbx($config) ?>">
    <input class="custom-control__input" type="checkbox" value="Y" <?= ($arParams['IS_CHECKED'] ? 'checked' : '') ?>
           name="<?= htmlspecialcharsbx($arParams['INPUT_NAME']) ?>">
    <span class="custom-control__indicator icon icon_checked"></span>
    <span class="custom-control__name"><?= $arResult['INPUT_LABEL'] ?>
</label>