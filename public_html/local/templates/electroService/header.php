<?

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Context;
use \Bitrix\Main\Page\Asset;
use ZLabs\DeferredFunction;

/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 */


if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$context = Context::getCurrent();
$request = $context->getRequest();
$server = $context->getServer();
$asset = Asset::getInstance();



?>
<!doctype html>
<html lang="<?= $context->getLanguage() ?>" class="document">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1240">
    <title><?php $APPLICATION->ShowTitle() ?></title>
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://es.zandi.tmweb.ru/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://es.zandi.tmweb.ru/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://es.zandi.tmweb.ru/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://es.zandi.tmweb.ru/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="http://es.zandi.tmweb.ru/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="http://es.zandi.tmweb.ru/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="http://es.zandi.tmweb.ru/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="http://es.zandi.tmweb.ru/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="http://es.zandi.tmweb.ru/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="http://es.zandi.tmweb.ru/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="http://es.zandi.tmweb.ru/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="http://es.zandi.tmweb.ru/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="http://es.zandi.tmweb.ru/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="Главная  | МУП Электросервис Сахалин"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="http://es.zandi.tmweb.ru/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="http://es.zandi.tmweb.ru/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="http://es.zandi.tmweb.ru/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="http://es.zandi.tmweb.ru/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="http://es.zandi.tmweb.ru/mstile-310x310.png" />
    <?php
    ZLabs\JSCore::addBlocks(['common']);
    $APPLICATION->ShowHead();
    ?>
</head>
<body class="body">
<?php
$APPLICATION->ShowPanel();
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:system.auth.form",
    "auth.modal",
    Array(
        "COMPONENT_TEMPLATE" => "auth.modal",
        "FORGOT_PASSWORD_URL" => "",
        "PROFILE_URL" => "/сonsumers/profile/",
        "REGISTER_URL" => "",
        "SHOW_ERRORS" => "N"
    )
);?>
<header class="header">
    <div class="container">
        <div class="header__item">
            <?php if (!CSite::InDir('/index.php')) : ?>
                <a href="/" title="<? $APPLICATION->ShowTitle() ?>" class="link header__brand clearfix">
                    <span class="header__logo"></span>
                    <span class="header-brand__text">МУП<br>«Электросервис»</span>
                </a>
            <?php else : ?>
                <span class="link header__brand clearfix">
                    <span class="header__logo"></span>
                    <span class="header-brand__text">МУП<br>«Электросервис»</span>
                </span>
            <?php endif; ?>

        </div>
        <div class="header__item">

                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/included_areas/header/tel1.php"
                    )
                );?>

        </div>
        <div class="header__item">

            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/local/included_areas/header/tel2.php"
                )
            );?>

        </div>
        <div class="header__item">
            <div class="header__account">
                <?if ($USER->IsAuthorized()) :?>
                    <a href="/personal/" class="link header-account__link icon icon_user icon_arrow-angle-down_after header-account__icon"
                       title="Личный кабинет">Личный кабинет</a>
                        <div class="header-account__sub">
                            <ul class="header-account__list">
                                <li class="header-account__item"><a href="/personal/make-order/" class="link header-account-item__link icon icon_list header-account-item__icon" title="Подать заявку">Подать заявку</a></li>
                                <li class="header-account__item"><a href="/personal/my-order/" class="link header-account-item__link icon icon_file header-account-item__icon" title="Мои заявки">Мои заявки</a></li>
                                <li class="header-account__item"><a href="/personal/in-work-order/" class="link header-account-item__link icon icon_clock header-account-item__icon" title="Заявки в работе">Заявки в работе</a></li>
                                <li class="header-account__item"><a href="/personal/complete-order/" class="link header-account-item__link icon icon_list-checked header-account-item__icon" title="Завершенные заявки">Завершенные заявки</a></li>
                                <li class="header-account__item"><a href="#calc" class="link header-account-item__link header-account-item_calc icon icon_divide header-account-item__icon" title="Калькулятор">Калькулятор</a></li>
                                <li class="header-account__item"><a href="/personal/settings/" class="link header-account-item__link icon icon_gear header-account-item__icon" title="Настройки">Настройки</a></li>
                                <li class="header-account__item"><a href="?logout=yes" class="link header-account-item__link icon icon_door header-account-item__icon" title="Выход">Выход</a></li>
                            </ul>
                        </div>
                <?else :?>
                    <a href="/personal/" class="link header-account__link icon icon_key header-account__icon"
                       title="Войти в кабинет">Войти в кабинет</a>
                <?endif;?>
            </div>
        </div>
    </div>
</header>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/local/included_areas/header/calc.php"
    )
);?>
<nav class="nav">
    <div class="container">

            <?$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "top.menu",
    Array(
        "ALLOW_MULTI_SELECT" => "N",
        "CHILD_MENU_TYPE" => "left",
        "COMPONENT_TEMPLATE" => "top.menu",
        "DELAY" => "N",
        "MAX_LEVEL" => "2",
        "MENU_CACHE_GET_VARS" => array(),
        "MENU_CACHE_TIME" => "120000",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "ROOT_MENU_TYPE" => "top",
        "USE_EXT" => "N"
    )
);?>
        </ul>
        <div class="nav__search">
            <span class="icon icon_magnifier nav-search__icon"></span>
            <div class="nav-search-group">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:search.form",
                    "search.form",
                    Array(
                        "PAGE" => "#SITE_DIR#search/",
                        "USE_SUGGEST" => "N"
                    )
                );?>
            </div>
        </div>
    </div>
</nav>
<?=DeferredFunction::showHeaderAndBreadcrumbsWrapper('title')?>
<main class="main <?= DeferredFunction::showWrapperClass('container') ?>">

