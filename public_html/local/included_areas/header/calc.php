<?

use Bitrix\Main\Loader;

if (!Loader::includeModule("iblock")) {
    $this->abortResultCache();
    ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
    return;
}

$arConst = [];
$arSections = [];

$arFilter = array('IBLOCK_ID' => IBLOCK_CALC_ID, 'GLOBAL_ACTIVE' => 'Y');
$db_list = CIBlockSection::GetList(array(), $arFilter, true, [
    'ID',
    'IBLOCK_ID',
    'NAME',
    'UF_*'
]);

while ($ar_result = $db_list->GetNext()) {
    $arSections[] = $ar_result;
}

$arSelect = array("ID", "IBLOCK", "NAME", "PROPERTY_CONSTANT_VALUE", "IBLOCK_SECTION_ID");
$arFilter = array("IBLOCK_ID" => IBLOCK_CALC_ID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
$res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 50), $arSelect);
while ($ob = $res->GetNext()) {
    if ($ob["IBLOCK_SECTION_ID"]) {
        foreach ($arSections as $sectionId => $section) {
            if ($section['ID'] === $ob['IBLOCK_SECTION_ID']) {
                if (!isset($arConst['sections'][$ob["IBLOCK_SECTION_ID"]]['from'])) {
                    $arConst['sections'][$ob["IBLOCK_SECTION_ID"]] = [
                        "from" => $section['UF_FROM_VALUE'],
                        "to" => $section['UF_TO_VALUE']
                    ];
                }
                $arConst['sections'][$ob["IBLOCK_SECTION_ID"]]['const'][] = [
                    "name" => $ob["NAME"],
                    "val" => $ob["PROPERTY_CONSTANT_VALUE_VALUE"],
                ];
                break;
            }
        }
    } else {
        $arConst['const'][] = [
            "name" => $ob["NAME"],
            "val" => $ob["PROPERTY_CONSTANT_VALUE_VALUE"],
        ];
    }
}


$arConst = json_encode($arConst);
?>

<div class="calc visibility" id="calc" data-const='<? if ($arConst) echo $arConst; ?>'>
    <div class="calc__form feedback-form">
        <div class="feedback-form__title">
            <div class="feedback-form__title-value calc__title-value">Калькулятор<br>расчета стоимости</div>
        </div>
        <div class="feedback-form feedback-form_tall">
            <div class="feedback-form__group calc__form-group">
                <span class="feedback-form__label">Тип присоединения</span>
                <input type="hidden" name="CONNECTION"
                       class="feedback-form__control feedback-form__control_type_list feedback-form__control_type_list_hidden "
                       title="Выберите тип присоединения" value="" id="connection">
                <div class="feedback-form__control_type_list_pseudo"><span
                            class="feedback-form__control_type_current-option">Выберите тип присоединения</span><span
                            class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                <div class="feedback-form__control_type_options nano">
                    <ul class="feedback-form__options-container nano-content">
                        <li data-value="Существующее" class="feedback-form__control_type_option">Существующее</li>
                        <li data-value="Новое" class="feedback-form__control_type_option">Новое</li>
                    </ul>
                </div>
            </div>
            <div class="feedback-form__group calc__form-group">
                <span class="feedback-form__label">Максимальная приосединяемая мощность, кВт</span>
                <span class="pop-up__trigger">?<span
                            class="pop-up pop-up_grey pop-up_above pop-up__hint">От 1 до 30 кВт</span></span>
                <div class="feedback-form__range" data-min="1" data-max="30"></div>
                <div class="feedback-form__range-list">
                    <span class="feedback-form__range-item" data-position="0%">1</span>
                    <span class="feedback-form__range-item" data-position="50%">15</span>
                    <span class="feedback-form__range-item" data-position="100%">30</span>
                </div>
            </div>
            <div class="feedback-form__group calc__form-group clearfix">
                <div class="feedback-form__group calc__form-group calc__form-group_small">
                    <label for="show" class="feedback-form__label feedback-form__label_middle">Выберите значение
                        ползунком<br>или введите его в ручную</label>

                </div>
                <div class="feedback-form__group calc__form-group calc__form-group_small">
                    <input type="text" min="1" max="30" name="SHOW"
                           class="feedback-form__control feedback-form__control_type_text" id="show">
                </div>
            </div>
            <div class="calc__form-group calc__form-group calc__form-group_big-offset clearfix">
                <div class="feedback-form__group calc__form-group calc__form-group_small">
                    <span class="feedback-form__label">Категория надёжности</span>
                    <input type="hidden" name="CALC_RELIABILITY"
                           class="feedback-form__control feedback-form__control_type_list feedback-form__control_type_list_hidden "
                           title="Выберите категорию надёжности" id="calc_reliability" value="2">
                    <div class="feedback-form__control_type_list_pseudo"><span
                                class="feedback-form__control_type_current-option">2</span><span
                                class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                    <div class="feedback-form__control_type_options nano">
                        <ul class="feedback-form__options-container nano-content">
                            <li class="feedback-form__control_type_option" data-value="2">2</li>
                            <li class="feedback-form__control_type_option" data-value="3">3</li>
                        </ul>
                    </div>
                </div>
                <div class="feedback-form__group calc__form-group calc__form-group_small">
                    <span class="feedback-form__label">Уровень напряжения (кВ)</span>
                    <input type="hidden" name="VOLTAGE"
                           class="feedback-form__control feedback-form__control_type_list feedback-form__control_type_list_hidden "
                           title="Выберите уровень напряжения" value="0.4" id="voltage">
                    <div class="feedback-form__control_type_list_pseudo"><span
                                class="feedback-form__control_type_current-option">0.4</span><span
                                class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                    <div class="feedback-form__control_type_options nano">
                        <ul class="feedback-form__options-container nano-content">
                            <li class="feedback-form__control_type_option" data-value="0.4">0.4</li>
                            <li class="feedback-form__control_type_option" data-value="6">6</li>
                            <li class="feedback-form__control_type_option" data-value="10">10</li>
                            <li class="feedback-form__control_type_option" data-value="15">15</li>
                            <li class="feedback-form__control_type_option" data-value="20">20</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="feedback-form__group calc__form-group">
                <label for="distance" class="feedback-form__label">Расстояние до ближайших электросетевых объектов,
                    м</label>
                <input type="text" id="distance" name="DISTANCE"
                       class="feedback-form__control feedback-form__control_type_text"
                       placeholder="Введите значение">
            </div>
            <div class="calc__form-group calc__form-group clearfix">
                <div class="feedback-form__group calc__form-group calc__form-group_small">
                    <button class="calc-form__btn">
                        <span class="calc-form__btn-link">Расчитать стоимость</span>
                    </button>
                </div>
                <div class="feedback-form__group calc__form-group calc__form-group_small">
                    <div class="calc__total">
                        <div class="feedback-form__label">Плата по ставке за мощность</div>
                        <div class="calc-total__value-and-currency">
                            <span class="calc__total-value">0</span> <span class="calc__total-currency">Ь</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
