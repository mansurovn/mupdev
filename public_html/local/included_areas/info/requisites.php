<table class="info__table info__table_2-col">
    <tbody>
    <tr>
        <td>
            Банк получателя
        </td>
        <td>
            Филиал Банка ВТБ (ПАО)<br>
            в г. Хабаровске
        </td>
    </tr>
    <tr>
        <td>
            Расчетный счет
        </td>
        <td>
            40702810208020009601
        </td>
    </tr>
    <tr>
        <td>
            Корр. счет
        </td>
        <td>
            30101810400000000727
        </td>
    </tr>
    <tr>
        <td>
            БИК
        </td>
        <td>
            040813727
        </td>
    </tr>
    </tbody>
</table>