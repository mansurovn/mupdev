<?php

namespace ZLabs;

use Bitrix\Main\Context;

class SmartSiteStub
{
    private $allowableGroupsOfUsers = [7];

    protected $pathToStub;
    protected $siteId;

    public function __construct()
    {
        $this->siteId = SITE_ID;
        $this->pathToStub = $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/' . $this->siteId . '/site_closed.php';
    }

    public static function onBitrixPrologHandler()
    {
        $smartSiteStub = new self();
        if ($smartSiteStub->isNeedShowStub()) {
            $pathToStub = $smartSiteStub->getPathToStub();
            if (file_exists($pathToStub)) {
                require_once($pathToStub);
                die();
            }
        }
    }

    protected function isNeedShowStub()
    {
        if (is_callable(array($this, $this->siteId . 'IsNeedShowStub'))) {
            return call_user_func(array($this, $this->siteId . 'IsNeedShowStub'));
        }
        return false;
    }

    protected function s1IsNeedShowStub()
    {
        global $USER;
        $server = Context::getCurrent()->getServer();

        return false;
        return !$USER->IsAdmin() &&  !\CSite::InGroup($this->allowableGroupsOfUsers);
    }

    protected function getPathToStub()
    {
        return $this->pathToStub;
    }
}