<?php

namespace ZLabs\HighLoadBlock;


use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\LoaderException;

abstract class HighLoadBlockDecorator
{
    protected $entityName;
    protected $highLoadBlock;
    protected $entity;

    function __construct()
    {
        if (!$this->entityName) {
            new \Exception('Empty entity name');
        }
        if ($this->includeModule()) {
            $this->highLoadBlock = HL\HighloadBlockTable::getList(['filter' => ['NAME' => $this->entityName]])->fetch();
            $this->entity = HL\HighloadBlockTable::compileEntity($this->highLoadBlock);
        } else {
            new LoaderException('Module highloadblock not installed');
        }
    }

    protected function includeModule()
    {
        return Loader::includeModule('highloadblock');
    }

    public function getId()
    {
        return $this->highLoadBlock['ID'];
    }

    public function getEntityDataClass()
    {
        return $this->entity->getDataClass();
    }
}