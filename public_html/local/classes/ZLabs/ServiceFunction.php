<?php

namespace ZLabs;


class ServiceFunction
{
    public static function Pre($var, $forAll = false, $doDie = false)
    {
        if (self::IsAdmin($forAll)) : ?>
            <div style="text-align: left; font-size: 10px">
                <pre><? print_r($var) ?></pre>
            </div>
        <? endif;
        if ($doDie) {
            die;
        }
    }

    private static function IsAdmin($forAll = false)
    {
        global $USER;
        if ($USER->IsAdmin() || $forAll)
            return true;
        else
            return false;
    }

    public static function EndingsForm($n, $form1, $form2, $form5)
    {
        $n = abs($n) % 100;
        $n1 = $n % 10;
        if ($n > 10 && $n < 20) return $form5;
        if ($n1 > 1 && $n1 < 5) return $form2;
        if ($n1 == 1) return $form1;
        return $form5;
    }
}