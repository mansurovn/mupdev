<?

namespace ZLabs;


class DeferredFunction
{
    const NOT_SHOW_H1_PROP = 'not_show_h1';
    const NOT_SHOW_NAV_CHAIN_IN_HEADER_PROP = 'not_show_nav_chain_in_header';
    const NOT_SHOW_WRAPPER_CLASS_PROP = 'not_show_wrapper_class';
    const NOT_SHOW_BREADCRUMBS_AND_HEADER_WRAPPER = 'not_show_breadcrumbs_and_header_wrapper';

    static function getH1($cssClass = '')
    {
        return ('Y' !== $GLOBALS['APPLICATION']->GetProperty(self::NOT_SHOW_H1_PROP, ''))
            ? '<h1' . (!empty($cssClass) ? " class=\"$cssClass\"" : '') . '>'
            . $GLOBALS['APPLICATION']->GetTitle() . '</h1>'
            : '';
    }

    static function showH1($cssClass = '')
    {
        return $GLOBALS['APPLICATION']->AddBufferContent(array(\ZLabs\DeferredFunction::class, 'getH1'), $cssClass);
    }

    static function getNavChain()
    {
        return ('Y' !== $GLOBALS['APPLICATION']->GetProperty(self::NOT_SHOW_NAV_CHAIN_IN_HEADER_PROP, ''))
            ? $GLOBALS['APPLICATION']->GetNavChain() : '';
    }

    static function showNavChain()
    {
        return $GLOBALS['APPLICATION']->AddBufferContent(array(\ZLabs\DeferredFunction::class, 'getNavChain'));
    }

    static function getWrapperClass($cssClass = '')
    {
        return ('Y' !== $GLOBALS['APPLICATION']->GetProperty(self::NOT_SHOW_WRAPPER_CLASS_PROP, ''))
            ? $cssClass : '';
    }


    static function showWrapperClass($cssClass = '')
    {
        return $GLOBALS['APPLICATION']->AddBufferContent(array(\ZLabs\DeferredFunction::class, 'getWrapperClass'), $cssClass);
    }

    static function getHeaderAndBreadcrumbsWrapper()
    {
        $header = static::getH1('h1');
        $breadcrumbs = static::getNavChain();

        return ('Y' !== $GLOBALS['APPLICATION']->GetProperty(self::NOT_SHOW_BREADCRUMBS_AND_HEADER_WRAPPER, ''))
            ? '<section class="title">
                    <div class="container">' .
                        $header .
                        $breadcrumbs .
                    '</div>
                </section>' : '';
    }

    static function showHeaderAndBreadcrumbsWrapper($cssClass = '')
    {
        return $GLOBALS['APPLICATION']->AddBufferContent(array(\ZLabs\DeferredFunction::class, 'getHeaderAndBreadcrumbsWrapper'), $cssClass);
    }
}
