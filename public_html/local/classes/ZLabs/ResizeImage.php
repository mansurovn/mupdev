<?php

namespace ZLabs;

class ResizeImage
{
    const PROPORTIONAL_RATIO_LEFT_BORDER = 1.2;
    const PROPORTIONAL_RATIO_RIGHT_BORDER = 1.6;

    protected $width;
    protected $height;
    protected $method;
    protected $uploadDirName;
    protected $noImageSrc;
    protected $arImages;
    protected $checkRatio = true;

    public function __construct($width, $height, $method = BX_RESIZE_IMAGE_EXACT)
    {
        $this->width = $width;
        $this->height = $height;
        $this->method = $method;

        $this->uploadDirName = \COption::GetOptionString('main', 'upload_dir', 'upload');
    }

    public function setCheckRatio(bool $bCheckRatio)
    {
        $this->checkRatio = $bCheckRatio;
    }

    public function setNoImageSrc($noImageSrc)
    {
        $this->noImageSrc = $noImageSrc;
    }

    public function getNoImageFile()
    {
        return array(
            'SRC' => $this->noImageSrc,
            'WIDTH' => $this->width,
            'HEIGHT' => $this->height,
        );
    }

    public function getImageIProperty(&$image, $entityPrefix, $arIPropertyValues, $alternativeValue)
    {
        if (is_array($image)) {
            /**
             * todo: разобратсья что происходит здесь
             */
            /*if (isset($image['SAFE_SRC'])) {
                $image['UNSAFE_SRC'] = $image['SRC'];
                $image['SRC'] = $image['SAFE_SRC'];
            } else {
                $image['UNSAFE_SRC'] = $image['SRC'];
                $image['SRC'] = \CHTTP::urnEncode($image['SRC'], 'UTF-8');
            }*/
            $image['ALT'] = '';
            $image['TITLE'] = '';
            if (isset($arIPropertyValues) && is_array($arIPropertyValues)) {
                if (isset($arIPropertyValues[$entityPrefix . '_FILE_ALT'])) {
                    $image['ALT'] = $arIPropertyValues[$entityPrefix . '_FILE_ALT'];
                }
                if (isset($arIPropertyValues[$entityPrefix . '_FILE_TITLE'])) {
                    $image['TITLE'] = $arIPropertyValues[$entityPrefix . '_FILE_TITLE'];
                }
            }
            if ($image['ALT'] == '' && isset($alternativeValue)) {
                $image['ALT'] = $alternativeValue;
            }
            if ($image['TITLE'] == '' && isset($alternativeValue)) {
                $image['TITLE'] = $alternativeValue;
            }
        }
    }

    public function resize(&$image)
    {
        if (is_array($image)) {
            $image['REAL_SRC'] =
                $image['SRC'] ?: '/' . $this->uploadDirName . '/' . $image['SUBDIR'] . '/' . $image['FILE_NAME'];

            $resizePicture = \CFile::ResizeImageGet(
                $image,
                array('width' => $this->width, 'height' => $this->height),
                $this->checkProportionalResizeMethod($image['WIDTH'], $image['HEIGHT'])
                    ? $this->method : BX_RESIZE_IMAGE_PROPORTIONAL,
                true
            );
            if ($resizePicture['src']) {
                $image['SRC'] = $resizePicture['src'];
                $image['WIDTH'] = $resizePicture['width'];
                $image['HEIGHT'] = $resizePicture['height'];
            } else {
                $image['SRC'] = $image['REAL_SRC'];
            }
        } elseif ($this->noImageSrc) {
            $image['SRC'] = $this->noImageSrc;
            $image['WIDTH'] = $this->width;
            $image['HEIGHT'] = $this->height;
        }

        $image['IS_SMALL'] = $image['WIDTH'] < $this->width ? 'Y' : 'N';
    }

    protected function checkProportionalResizeMethod($width, $height)
    {
        if (!$this->checkRatio) {
            return true;
        }
        $ratio = $width / $height;
        return $ratio > static::PROPORTIONAL_RATIO_LEFT_BORDER && $ratio < static::PROPORTIONAL_RATIO_RIGHT_BORDER;
    }

    public function resizeImagesFromDisplayProp(
        $arImagesFromDisplayProp,
        $entityPrefix = false,
        $arIPropertyValues = array(),
        $elementName = false
    ) {
        $arResizeImages = array();
        if ($arImagesFromDisplayProp) {
            $this->fromDisplayProp($arImagesFromDisplayProp, $entityPrefix, $arIPropertyValues, $elementName);
            $this->resizeImages();
            $arResizeImages = $this->getImages();
            $this->clearImages();
        }
        return $arResizeImages;
    }

    protected function fromDisplayProp(
        $arImagesFromDisplayProp,
        $entityPrefix = false,
        $arIPropertyValues = array(),
        $elementName = false
    ) {
        if (is_array($arImagesFromDisplayProp['DISPLAY_VALUE'])) {
            foreach ($arImagesFromDisplayProp['FILE_VALUE'] as $fileValue) {
                $imageData = $fileValue;
                if ($entityPrefix && $elementName) {
                    $this->getImageIProperty($imageData, $entityPrefix, $arIPropertyValues, $elementName);
                }
                $this->arImages[] = $imageData;
            }
        } else {
            $imageData = $arImagesFromDisplayProp['FILE_VALUE'];
            if ($entityPrefix && $elementName) {
                $this->getImageIProperty($imageData, $entityPrefix, $arIPropertyValues, $elementName);
            }
            $this->arImages[] = $imageData;
        }
    }

    protected function resizeImages()
    {
        foreach ($this->arImages as &$arImage) {
            $this->resize($arImage);
        }
        unset($arImage);
    }

    protected function getImages()
    {
        return $this->arImages;
    }

    protected function clearImages()
    {
        $this->arImages = array();
    }
}
