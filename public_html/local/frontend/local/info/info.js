$(document).ready(function () {
    var selectBox = $('.info__select-box');
    var selectBoxList = $('.info-select-box__list');
    //var selectBoxListContainer = $('.info-select-box-list__container');
    var toggle = $('.info-select-box__current');

    setTimeout(function () {
        selectBoxList.addClass('info-select-box__list_hide');
    }, 20);

    if(selectBoxList.length) {
        selectBoxList.nanoScroller({
            preventPageScrolling: true,
            sliderMaxHeight: 300
        });

        selectBoxList.each(function () {
            var height = 0;

            $(this).find('.info-select-box__item').each(function () {
                height += $(this).outerHeight();
            });

            if (height < 295) {
                selectBoxList.height(height + 15);
            }
            height = 0;
        });

        toggle.on('click', function(){
            var selectBox = $(this).closest('.info__select-box');
            var curToggle = $(this);
            var selectBoxList = selectBox.find('.info-select-box__list');
            //  var selectBoxListContainer = selectBox.find('.info-select-box-list__container');
            var selectBoxItem = selectBox.find('.info-select-box__item');

            selectBoxList.slideToggle('fast');
            selectBox.toggleClass('info__select-box_expand');

            selectBoxItem.on('click', function(){
                var curValue = $(this).children('.info-select-box__value').text();
                var curBlock = $(this).closest('.info__block');
                var curTable = curBlock.find('table');
                var item = curBlock.find('.info-table__item');
                var curItem = curBlock.find('.info-table__item[data-filter="' + curValue + '"]');

                curToggle.text(curValue);
                selectBoxItem.removeClass('info-select-box__item_active');
                $(this).addClass('info-select-box__item_active');
                selectBoxList.slideUp('fast');
                selectBox.removeClass('info__select-box_expand');

                item.removeClass('info-table__item_in');
                setTimeout(function () {
                    item.removeClass('info-table__item_active');
                    curItem.addClass('info-table__item_active');
                    curBlock.height(curBlock.children('.info__block-container').height());
                }, 300);
                setTimeout(function () {
                    curItem.addClass('info-table__item_in');
                }, 320);
            });
            selectBox.on('mouseleave', function(){
                selectBoxList.slideUp('fast');
                selectBox.removeClass('info__select-box_expand');
            });
        })
    }
});