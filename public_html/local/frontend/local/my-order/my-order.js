$(document).ready(function () {
    var params = window
        .location
        .search
        .replace('?','')
        .split('&')
        .reduce(
            function(p,e){
                var a = e.split('=');
                p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
                return p;
            },
            {}
        );

    if (params['SUCCESS'] == "Y") {
        $.fancybox.open($('#calc__success-message'), {
            padding: 0,
            caption: '',
            touch: false,
            baseClass: 'fancybox-custom',
            lang: 'ru',
            btnTpl: {
                smallBtn: '<button data-fancybox-close class="icon icon_multiply calc__close" title="{{CLOSE}}"></button>'
            },
            i18n: {
                'ru': {
                    CLOSE: 'Закрыть'
                }
            }
        });
    }
});