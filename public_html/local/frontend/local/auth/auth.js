$(document).ready(function () {
    var link = $('.auth__link');

    if (link.length) {
        link.fancybox({
        selector: '.auth__link',
        padding: 0,
        caption: '',
        touch: false,
        baseClass: 'fancybox-custom',
        lang: 'ru',
        btnTpl: {
            smallBtn: '<div data-fancybox-close class="icon icon_multiply feedback-form__close" title="Закрыть"></div>'
        },
        i18n: {
            'ru': {
                CLOSE: 'Закрыть'
            }
        }
    });
    }
});