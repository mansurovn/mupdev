$(document).ready(function () {
    var link = $('.application-form__btn, .application-form__submit');
    var panelContainer = $('.application__tabpanels');
    var marker = $('.application__tab');
    var ppCheck = $('.custom-control__input');
    var form = $('.application__form');

    ppCheck.on('change', function (event) {
        $(this).closest('form').find('.application-form__submit').prop("disabled", !$(this).prop('checked'));
    });

    setTimeout(function () {
        $('.application__tabpanel').addClass('application__tabpanel_ready') ;
    }, 50);

    link.on('click', function (event) {
        var nextPanel = $($(this).attr('href'));
        var curPanel = $(this).closest('.application__tabpanel');
        var curFields = curPanel.find(':not(.calc .feedback-form__control):not(.feedback-form__control_inactive).feedback-form__control');
        var check = true;

        if (!$(this).hasClass('application-form__btn_prev')) {
            curFields.on('change', function (event) {
                fieldCheck($(this));
            });

            curFields.each(function(){
                fieldCheck($(this));
            });
        }

        if (check) {
            curFields.off('change');

            if ($(this).hasClass('application-form__submit')) {
                $(this).closest('.application__form').trigger('submit');
            } else {
                panelContainer.find('.application__tabpanel').each(function () {
                    var prevPanel = $(this);
                    if (prevPanel.hasClass('application__tabpanel_active')) {
                        prevPanel.removeClass('application__tabpanel_in');
                        setTimeout(function () {
                            prevPanel.removeClass('application__tabpanel_active');
                        }, 400);
                    }
                });
                setTimeout(function () {
                    nextPanel.addClass('application__tabpanel_active');
                    panelContainer.height(nextPanel.height());
                    setTimeout(function () {
                        nextPanel.addClass('application__tabpanel_in');
                    }, 20);
                }, 400);

                marker.removeClass('application__tab_active');
                $('.application__tab[data-form="' + $(this).attr('href').substr(-1) + '"]').addClass('application__tab_active');
            }
        } else {
            event.preventDefault();
        }

        function fieldCheck(field) {
            if (field.val() === "") {
                field.closest('.feedback-form__group').addClass('feedback-form__group_has-error');

                check = false;
            } else {
                field.closest('.feedback-form__group').removeClass('feedback-form__group_has-error');
            }
        }
    });


    $("input[name='PERSON_TYPE']").on("change", function(){
        var PERSON_TYPE = $(this).val();
        //Показываю свои наборы полей для определённого юр. лица
        if(PERSON_TYPE == 1){ //физ. лицо
            $(".feedback-form__fiz").show().find('.feedback-form__control').removeClass('feedback-form__control_inactive');
            $(".feedback-form__yur").hide().find('.feedback-form__control').addClass('feedback-form__control_inactive');
        }
        if(PERSON_TYPE == 2){ //юр. лицо
            $(".feedback-form__fiz").hide().find('.feedback-form__control').addClass('feedback-form__control_inactive');;
            $(".feedback-form__yur").show().find('.feedback-form__control').removeClass('feedback-form__control_inactive');
        }
    });
    //Работа с файлами
    $(".feedback-form__pseudo-file-control").click(function(){
        $(this).prev("input[type=file]").trigger('click');
        event.preventDefault();

    });

    $("input.feedback-form__control_type_file").change(function(){
        var cur_file_name = $(this).val();
        cur_file_name = cur_file_name.split("\\");
        cur_file_name = cur_file_name[cur_file_name.length-1];

        $(this).next(".feedback-form__pseudo-file-control").html("Заменить: " + cur_file_name).removeClass("novalidtext");
    })
});