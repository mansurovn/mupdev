$(document).ready(function () {
    setTimeout(function () {
        $('.calc').addClass('calc_ready');
    }, 20);

    var connection = $('#connection');              // тип присоединения (input)
    var show = $('#show');      // Максимальная приосединяемая мощность, кВт  (input)
    var reliability = $('#calc_reliability');        // Категория надежности  (input)
    var voltage = $('#voltage');                // Уровень напряжения  (input)
    var distance = $('#distance');          // Расстояние до ближайших электросетевых объектов, м  (input)
    var total  = $('.calc__total-value');   // Итог  (span)

    var btn = $('.calc-form__btn'); ///(button)

    var constants = $('.calc').data('const');

    btn.on('click', function (event) {
        event.preventDefault();

        // Код вызова сообщения об успешной отправке

        var C1 = constants.const[0].val; // их коэф.
        var X = show.val(); // максимальная мощность
        var Y = distance.val(); // расстояние
        var C = 0;
        var C2 = 0;
        var C3 = 0;
        var C4 = 0;

        if(Y > 300){
            for (var key in constants.sections) {
                if (X > parseFloat(constants.sections[key].from) && X <= parseFloat(constants.sections[key].to)) {
                    C2 = constants.sections[key].const[0].val;
                    C3 = constants.sections[key].const[1].val;
                    C4 = constants.sections[key].const[2].val;
                }
            }
        }

        if (!isNaN(distance.val())) {}

        if(connection.val() == "Существующее"){
            C = X*C1;
        }else{

            if(voltage.val() == 0.4 && reliability.val() == 3){
                C = X*C1 + Y*C3 + X*C4;
            }
            if(voltage.val() == 0.4 && reliability.val() == 2){
                C = X*C1 + ((Y*C3 + X*C4)+(Y*C3 + X*C4));
            }
            if(voltage.val() >= 6 && reliability.val() == 3){
                C = X*C1 + Y*C3;
            }
            if(voltage.val() >= 6 && reliability.val() == 2){
                C = X*C1 + (Y*C3 + Y*C3);
            }
        }
        if(C > 0){
            $(".calc__total-value").html(Math.round(C));
        }

    });

    $('body').fancybox({
        selector: '.application__calc-link, .header-account-item_calc, .sidebar__link_calc',
        padding: 0,
        caption: '',
        touch: false,
        baseClass: 'fancybox-custom',
        lang: 'ru',
        btnTpl: {
            smallBtn: '<button data-fancybox-close class="icon icon_multiply calc__close" title="{{CLOSE}}"></button>'
        },
        i18n: {
            'ru': {
                CLOSE: 'Закрыть'
            }
        }
    });
});