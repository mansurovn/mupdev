$(document).ready(function () {
    var map = $('.contacts-map__canvas');
    var latDefault = 46.933415;
    var lonDefault = 142.730692;

    if (window.ymaps) {
        ymaps.ready(function () {
            map = new ymaps.Map('YMap', {
                center: [latDefault, lonDefault],
                zoom: 17,
                //controls: ['zoomControl', 'searchControl', 'GeolocationControl', 'RouteEditor'],
                controls: [],
                duration: 1000,
                timingFunction: "ease-in",
                flying: true,
                safe: true
            });

            mark = new ymaps.Placemark([latDefault, lonDefault], {
                hintContent: 'улица Ленина, 378А',
                //balloonContent: 'улица Ленина, 378А'
            });
            map.geoObjects.add(mark);

            map.controls.add(new ymaps.control.ZoomControl());
            /*map.controls.add(new ymaps.control.SearchControl());
            map.controls.add(new ymaps.control.GeolocationControl());
            map.controls.add(new ymaps.control.RouteEditor());*/

            map.behaviors.disable('scrollZoom');

            $('.contacts-map__preloader').fadeOut();
        });
    }
});

function yMapErrorHandler(err) {
    console.log('Error: cannot load Yandex Map');
    console.log(err);
}