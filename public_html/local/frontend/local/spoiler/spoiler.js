$(document).ready(function () {
    var spoiler = $('.spoiler');

    if (spoiler.length) {
        var trigger = $('.spoiler__trigger');
        var icon = $('.spoiler-item__icon');

        trigger.on('click', function (event) {
            var curSpoiler = $(this).closest('.spoiler');
            var hideArea = curSpoiler.find('.spoiler__hide-block');
            var curTrigger = curSpoiler.find('.spoiler__trigger');

            hideArea.slideToggle();
            curSpoiler.toggleClass('spoiler_active');
        });
    }
});