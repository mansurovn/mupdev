$(document).ready(function(){
    var images = $('[data-fancybox="images"]');

    if (images.length) {
        images.fancybox({
            selector: '[data-fancybox="images"]',
            thumbs: false,
            hash: false,
            infobar: true,
            lang: 'ru',
            i18n: {
                'ru': {
                    CLOSE: 'Закрыть',
                    NEXT: 'Следующий',
                    PREV: 'Предыдущий',
                    ERROR: 'Не удалось загрузить контент',
                    PLAY_START: 'Старт',
                    PLAY_STOP: 'Пауза',
                    FULL_SCREEN: 'Полный экран',
                    THUMBS: 'Миниатюры'
                }
            }
        });
    }
});