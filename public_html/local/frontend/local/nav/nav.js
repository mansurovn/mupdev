$(document).ready(function(){
    var nav__search = $('.nav__search');
    var trigger = $('.nav-search__icon');
    var input = $('.nav-search-group');

    if (nav__search.length) {
        trigger.on('click', function(){
            if (input.width() == 0) {
                input.animate({
                    width: '1150px',
                    opacity: 1
                }, "fast")
                    .trigger('focus');
                trigger.removeClass('icon_magnifier').addClass('icon_multiply');
            } else {
                input.animate({
                    width: '0',
                    opacity: 0
                }, "fast");
                trigger.removeClass('icon_multiply').addClass('icon_magnifier');
            }
            /*$(this).closest('.nav').on('mouseleave', function(){
                input.animate({
                    width: 0,
                    opacity: 0
                }, "fast")
            })*/
        });
    }
});