$(document).ready(function(){
    var toggle = $('.vacancies-vacancy__toggle');

    toggle.on('click', function(event){
        var block = $(this).closest('.vacancies__vacancy');
        var hide = block.find('.vacancies-vacancy__add');

        block.toggleClass('vacancies__vacancy_expand');
        hide.slideToggle();
        if (hide.css('display') === 'none') {
            $(this).children().text('Скрыть');
        } else {
            $(this).children().text('Подробнее');
        }
    });
});