<?php

namespace ZLabs\Components;

use Bitrix\Main\Loader;
use Bex\Bbc;
use ZLabs\JSCore;

/**
 * @global \CMain $APPLICATION
 */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!Loader::includeModule('bex.bbc')) {
    return false;
}

class FeedbackFormForm extends Bbc\Basis
{
    protected $jsBlocks = ['feedbackForm'];
    protected $cacheTemplate = false;

    public function onPrepareComponentParams($params)
    {
        $params['USE_AJAX'] = 'Y';
        $params['AJAX_TYPE'] = 'JSON';
        $params['AJAX_COMPONENT_ID'] = $params['ID'] ?: $this->randString();
        $params['GOALS'] = array_diff($params['GOALS'], array(''));

        return parent::onPrepareComponentParams($params);
    }

    public function executeMain()
    {
        if (!$this->isAjax()) {
            $this->obtainForm();
            JSCore::addBlocks($this->jsBlocks);
        } else {
            $this->doAction();
        }
    }

    protected function obtainForm()
    {
        $this->obtainFields();
        $this->obtainVisualParams();
    }

    protected function obtainFields()
    {
        if ($this->arParams['NUM_FIELDS'] > 0) {
            for ($i = 0; $i < $this->arParams['NUM_FIELDS']; $i++) {
                /**
                 * Основные параметры
                 */
                $this->arResult['FIELDS'][$i] = array(
                    'TYPE' => $this->arParams['FIELD_' . $i . '_TYPE'],
                    'CODE' => $this->arParams['FIELD_' . $i . '_CODE'],
                    'TITLE' => $this->arParams['FIELD_' . $i . '_TITLE'],
                );

                /**
                 * Если есть плейсхолдер
                 */
                if ($this->arParams['FIELD_' . $i . '_PLACEHOLDER']) {
                    $this->arResult['FIELDS'][$i]['PLACEHOLDER'] = $this->arParams['FIELD_' . $i . '_PLACEHOLDER'];
                }

                /**
                 * Значения для полей типа список, радио и чекбокс
                 */
                $possibleValues = $this->arParams['FIELD_' . $i . '_POSSIBLE_VALUES'];
                if ($possibleValues) {
                    foreach ($possibleValues as $fieldValue) {
                        if (!empty($fieldValue)) {
                            $this->arResult['FIELDS'][$i]['POSSIBLE_VALUES'][] = $fieldValue;
                        }
                    }
                }

                /**
                 * Для полей типа файл
                 */
                if ($this->arParams['FIELD_' . $i . '_TYPE'] == 'FILE'
                    && $this->arParams['FIELD_' . $i . '_MULTIPLE'] == 'Y') {
                    $this->arResult['FIELDS'][$i]['MULTIPLE'] = 'Y';
                }

                /**
                 * Для полей типа текст
                 */
                if ($this->arParams['FIELD_' . $i . '_TYPE'] == 'TEXT') {
                    $this->arResult['FIELDS'][$i]['MASK'] = (!empty($this->arParams['FIELD_' . $i . '_MASK']))
                        ? $this->arParams['FIELD_' . $i . '_MASK'] : 'SIMPLE';
                }
                if ($this->arParams['FIELD_' . $i . '_TYPE'] == 'TEXT') {
                    $this->arResult['FIELDS'][$i]['VALUE'] = (!empty($this->arParams['FIELD_' . $i . '_VALUE']))
                        ? $this->arParams['FIELD_' . $i . '_VALUE'] : '';
                }

                /**
                 * Обязательные поля
                 */
                if ($this->arParams['FIELD_' . $i . '_REQUIRE'] == 'Y') {
                    $this->arResult['REQUIRE_FIELDS'][] = $i;
                }

                /**
                 * Заметка поля
                 */
                if ($this->arParams['FIELD_' . $i . '_NOTE']) {
                    $this->arResult['FIELDS'][$i]['NOTE'] = $this->arParams['FIELD_' . $i . '_NOTE'];
                }

                /**
                 * Если передано значение поля
                 */
                if ($this->arParams['FIELD_VALUES'][$this->arParams['FIELD_' . $i . '_CODE']]) {
                    $this->arResult['FIELDS'][$i]['VALUE']
                        = $this->arParams['FIELD_VALUES'][$this->arParams['FIELD_' . $i . '_CODE']];
                }

               /*
                * Высота поля textarea
                */
                if ($this->arParams['FIELD_' . $i . '_TYPE'] == 'TEXTAREA') {
                    $this->arResult['FIELDS'][$i]['HEIGHT'] = (!empty($this->arParams['FIELD_' . $i . '_HEIGHT']))
                        ? $this->arParams['FIELD_' . $i . '_HEIGHT'] : 'DEFAULT';
                }
            }
            return true;
        }
        throw new \Exception('Не создано ни одно поле');
    }

    protected function obtainVisualParams()
    {
        $this->arResult['TITLE'] = $this->arParams['~TITLE'];
        $this->arResult['SUB_TITLE'] = $this->arParams['~SUB_TITLE'];
        $this->arResult['SUBMIT'] = $this->arParams['~SUBMIT'];
        $this->arResult['FOOTNOTE'] = $this->arParams['~FOOTNOTE'];
        $this->arResult['NUM_FIELDS'] = intval($this->arParams['NUM_FIELDS']);
        $this->arResult['POPUP_FORM'] = $this->arParams['POPUP_FORM'];
        if ($this->arParams['POPUP_FORM'] == 'Y') {
            $this->arResult['LINK_TO_FORM'] = $this->arParams['~LINK_TO_FORM'];
        }
    }

    protected function doAction()
    {
        if (is_callable(array($this, $this->request->get('action') . 'Action'))) {
            call_user_func(
                array($this, $this->request->get('action') . 'Action')
            );
        }
    }

    protected function submitAction()
    {
        $this->obtainFields();
        if ($this->arResult['FIELDS']) {
            $formData = '';
            $idFiles = array();
            $emailTo = $this->arParams['EMAIL_TO'];
            $arEventMessageId = $this->arParams['EVENT_MESSAGE_ID'];
            $formName = $this->arParams['NAME'];

            foreach ($this->arResult['FIELDS'] as $field) {
                if ($field['TYPE'] == 'FILE') {
                    $files = (array)$this->request->getFile($field['CODE']);
                    if (is_array($files['name'])) {
                        for ($i = 0; $i < count($files['name']); $i++) {
                            $file = array(
                                'name' => $files['name'][$i],
                                'size' => $files['size'][$i],
                                'tmp_name' => $files['tmp_name'][$i],
                                'type' => $files['type'][$i],
                                'del' => 'Y',
                                'MODULE_ID' => 'main'
                            );
                            $idFiles[] = \CFile::SaveFile($file, 'main');
                        }
                    } else {
                        $idFiles[] =
                            \CFile::SaveFile(
                                array_merge($files, array('del' => 'Y', 'MODULE_ID' => 'main')),
                                'main'
                            );
                    }
                } else {
                    $fieldValue = $this->request->get($field['CODE']);
                    if ($fieldValue) {
                        $formData .= $field['TITLE'] . ' ';
                        if (is_array($fieldValue)) {
                            for ($i = 0; $i < count($fieldValue); $i++) {
                                $formData .= $fieldValue[$i] . ($i + 1 == count($fieldValue) ? "\n" : ',');
                            }
                        } else {
                            $formData .= $fieldValue . "\n";
                        }
                    }
                }
            }

            if (!$formData) {
                $this->arResult['errors'][] = 'Данных с формы не поступило';
            }
            if (!$emailTo) {
                $this->arResult['errors'][] = 'Отсутствует email получателя формы';
            }

            if (!$arEventMessageId) {
                $this->arResult['errors'][] = 'Не найдены почтовые шаблоны, по которым необходимо отправить форму';
            }

            if (!$this->arResult['errors']) {
                foreach ($arEventMessageId as $eventMessageId) {
                    \CEvent::Send(
                        'ZLABS_FEEDBACK',
                        SITE_ID,
                        array(
                            'EMAIL_TO' => $emailTo,
                            'FORM_NAME' => $formName,
                            'DATA' => $formData
                        ),
                        'Y',
                        $eventMessageId,
                        $idFiles
                    );
                }

                $this->arResult['data']['ya_goals'] = $this->arParams['GOALS'];
                $this->arResult['data']['successMessage'] = $this->arParams['~SUCCESS_MESSAGE'];
                $this->arResult['data']['successMessageTitle'] = $this->arParams['~SUCCESS_MESSAGE_TITLE'];
                return;
            }
        }
        unset($this->arResult['FIELDS']);
    }

    public function returnDatas()
    {
        if (!$this->isAjax()) {
            parent::returnDatas();
        } else {
            echo json_encode($this->arResult);
        }
    }
}
