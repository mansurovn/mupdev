<?php
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<!--noindex-->

<script id="feedback-success-message" type="text/html">
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/' . $this->GetFolder() . '/feedback-success-message.mustache'); ?>
</script>
<section class="customer dealers-customer" id="dealers-form-container">
    <div class="container">
        <form class="feedback-form"
              id="<?= $arParams['AJAX_COMPONENT_ID'] ?>"
              enctype="multipart/form-data"
              method="post">
            <div class="content-box customer-wrap">
                <div class="content-box__stub content-box__stub_white">
                    <?php
                    if ($arResult['TITLE']): ?>
                        <h1 class="h1 customer__header dealers-customer__header"><?= $arResult['TITLE'] ?></h1>
                    <? endif; ?>
                    <div class="customer-form">

                        <?
                        foreach ($arResult['FIELDS'] as $fieldKey => $field) :
                            $requiredCssClass = in_array($fieldKey, $arResult['REQUIRE_FIELDS']) ? ' feedback-form__control_required' : '';
                            $placeholder = (in_array($fieldKey, $arResult['REQUIRE_FIELDS']) ? $field['PLACEHOLDER']
                                . '*' : $field['PLACEHOLDER']);
                            ?>
                            <div class="feedback-form__group">
                                <? switch ($field['TYPE']) :
                                    case 'TEXT' :
                                        $validCssClass = '';
                                        switch ($field['MASK']) {
                                            case 'PHONE':
                                                $validCssClass = ' feedback-form__control_valid_phone';
                                                break;
                                            case 'EMAIL':
                                                $validCssClass = ' feedback-form__control_valid_email';
                                                break;
                                            case 'TEXT_RU':
                                                $validCssClass = ' feedback-form__control_valid_text-ru';
                                                break;
                                            case 'PHONE_OR_EMAIL':
                                                $validCssClass = ' feedback-form__control_valid_phone_or_email';
                                                break;
                                        }
                                        ?>
                                        <input class="feedback-form__control feedback-form__control_type_text<?= $requiredCssClass
                                        . $validCssClass ?>"
                                               type="text"
                                               name="<?= $field['CODE'] ?>"
                                               value="<?= $field['VALUE'] ?>"
                                               placeholder="<?= $placeholder ?>">
                                        <? break;
                                    case 'TEXTAREA': ?>
                                        <textarea
                                                class="feedback-form__control feedback-form__control_type_textarea<?= $requiredCssClass ?>"
                                                name="<?= $field['CODE'] ?>"
                                                placeholder="<?= $placeholder ?>"><?= $field['VALUE'] ?></textarea>
                                        <? break;
                                    case 'LIST':
                                        if ($field['POSSIBLE_VALUES']) : ?>
                                            <select name="<?= $field['CODE'] ?>"
                                                    class="feedback-form__control feedback-form__control_type_list<?= $requiredCssClass ?>"
                                                    title="<?= $field['PLACEHOLDER'] ?>">
                                                <option value=""><?= $field['PLACEHOLDER'] ?></option>
                                                <? foreach ($field['POSSIBLE_VALUES'] as $fieldValue) : ?>
                                                    <option<?= $fieldValue == $field['VALUE']
                                                        ? ' selected' : '' ?>><?= $fieldValue ?></option>
                                                <? endforeach ?>
                                            </select>
                                            <?
                                        endif;
                                        break;
                                    case 'RADIO':
                                        if ($field['POSSIBLE_VALUES']) :
                                            foreach ($field['POSSIBLE_VALUES'] as $fieldValue): ?>
                                                <label class="feedback-form__label custom-control custom-control_type_radio"><input
                                                            class="custom-control__input feedback-form__control feedback-form__control_type_radio"
                                                            type="radio"
                                                            name="<?= $field['CODE'] ?>"
                                                            value="<?= $fieldValue ?>"<?= $fieldValue == $field['VALUE']
                                                        ? ' checked' : '' ?>>
                                                    <span class="custom-control__indicator"></span>
                                                    <span class="custom-control__name"><?= $fieldValue ?></span>
                                                </label>
                                            <? endforeach;
                                        endif;
                                        break;
                                    case 'CHECKBOX':
                                        if ($field['POSSIBLE_VALUES']) :
                                            foreach ($field['POSSIBLE_VALUES'] as $fieldValue): ?>
                                                <label class="feedback-form__label custom-control custom-control_type_checkbox"><input
                                                            class="custom-control__input feedback-form__control feedback-form__control_type_checkbox"
                                                            type="checkbox"
                                                            name="<?= $field['CODE'] ?>[]"
                                                            value="<?= $fieldValue ?>"<?= $fieldValue == $field['VALUE']
                                                        ? ' checked' : '' ?>>
                                                    <span class="custom-control__indicator"></span>
                                                    <span class="custom-control__name"><?= $fieldValue ?></span>
                                                </label>
                                            <? endforeach;
                                        endif;
                                        break;
                                    case 'FILE':
                                        $multiple = $field['MULTIPLE'] == 'Y';
                                        ?>
                                        <input class="feedback-form__control feedback-form__control_type_file<?= $requiredCssClass ?>"
                                               type="file"
                                               name="<?= $field['CODE'] . ($multiple ? '[]' : '') ?>"<?= $multiple ? ' multiple' : '' ?>>
                                        <a class="feedback-form__pseudo-file-control"
                                           href="#"><span class="feedback-form__link"><?= $placeholder ?></span></a>
                                        <div class="feedback-form__files-list visibility visibility_manual"></div>
                                        <? break;
                                    case 'HIDDEN': ?>
                                        <input type="hidden" name="<?= $field['CODE'] ?>"
                                               value="<?= $field['VALUE'] ?>">
                                        <? break;
                                endswitch;
                                if ($field['NOTE']) : ?>
                                    <div class="feedback-form__note"><?= $field['NOTE'] ?></div>
                                <? endif ?>
                            </div>
                            <?php
                        endforeach;
                        $disabledSubmit = ($arParams['USER_CONSENT'] === 'Y' && $arParams['USER_CONSENT_IS_CHECKED'] === 'N')
                            ? 'disabled="disabled"' : ''
                        ?>
                        <div class="feedback-form__footnote"><?= $arResult['FOOTNOTE'] ?></div>
                    </div>
                </div>
            </div>
            <div class="customer__submit-wrap">
                <button class="btn btn_main connect__btn feedback-form__submit customer__submit" type="submit"
                        name="submit"<?= $disabledSubmit ?>><?= $arResult['SUBMIT'] ?></button>
                <?php
                if ($arParams['USER_CONSENT'] === 'Y') : ?>
                    <div class="feedback-form__user-consent">
                        <?php
                        $APPLICATION->IncludeComponent('bitrix:main.userconsent.request', 'simple', Array(
                            'ID' => $arParams['USER_CONSENT_ID'],    // Соглашение
                            'IS_CHECKED' => $arParams['USER_CONSENT_IS_CHECKED'],    // Галка согласия проставлена по умолчанию
                            'AUTO_SAVE' => 'Y',    // Сохранять автоматически факт согласия
                            'IS_LOADED' => $arParams['USER_CONSENT_IS_LOADED'],    // Загружать текст соглашения сразу
                        ),
                            false
                        );
                        ?>
                    </div>
                    <?php
                endif; ?>
            </div>
        </form>
    </div>
</section>
<?php
if (!empty($arResult['LINK_TO_FORM']) && $arResult['POPUP_FORM'] == 'Y') : ?>
    <a href="#<?= $arParams['AJAX_COMPONENT_ID'] ?>"
       class="feedback-form-link"><?= $arResult['LINK_TO_FORM'] ?></a>
    <?php
endif; ?>
<script>
    $('#<?= $arParams['AJAX_COMPONENT_ID'] ?>').feedbackForm();
</script>
<!--/noindex-->