<?php

/** @var array $arCurrentValues */

use Bex\Bbc\Helpers\ComponentParameters;
use Bitrix\Iblock;
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!\Bitrix\Main\Loader::includeModule('bex.bbc')) {
    return false;
}

Loc::loadMessages(__FILE__);

try {

    $site = ($_REQUEST['site'] <> '' ? $_REQUEST['site'] : ($_REQUEST['src_site'] <> '' ? $_REQUEST['src_site'] : false));

    $arFilter = Array('TYPE_ID' => 'ZLABS_FEEDBACK', 'ACTIVE' => 'Y');
    if ($site !== false) {
        $arFilter['LID'] = $site;
    }

    $arEvent = Array();
    $dbType = CEventMessage::GetList($by = 'ID', $order = 'DESC', $arFilter);
    while ($arType = $dbType->GetNext()) {
        $arEvent[$arType['ID']] = '[' . $arType['ID'] . '] ' . $arType['SUBJECT'];
    }

    $arComponentParameters = array(
        'GROUPS' => array(
            'FORM_SUBMIT' => array(
                'NAME' => 'Отправка формы',
                'SORT' => 200
            ),
            'BACKEND_SETTING' => array(
                'NAME' => 'Технические настройки',
                'SORT' => 300
            ),
        ),
        'PARAMETERS' => array(
            'USER_CONSENT' => array(),
            'ID' => array(
                'PARENT' => 'BASE',
                'NAME' => 'ID формы',
                'TYPE' => 'STRING'
            ),
            'TITLE' => array(
                'PARENT' => 'BASE',
                'NAME' => 'Заголовок',
                'TYPE' => 'STRING',
                'ROWS' => 2
            ),
            'SUB_TITLE' => array(
                'PARENT' => 'BASE',
                'NAME' => 'Подзаголовок',
                'TYPE' => 'STRING',
                'ROWS' => 2
            ),
            'SUBMIT' => array(
                'PARENT' => 'BASE',
                'NAME' => 'Текст кнопки отправки',
                'TYPE' => 'STRING',
                'DEFAULT' => 'Отправить заявку',
                'ROWS' => 2
            ),
            'FOOTNOTE' => array(
                'PARENT' => 'BASE',
                'NAME' => 'Текст внизу формы',
                'TYPE' => 'STRING',
                'DEFAULT' => 'Поля отмеченные * обязательны для заполнения',
                'ROWS' => 2
            ),
            'NUM_FIELDS' => array(
                'PARENT' => 'BASE',
                'NAME' => 'Количество полей',
                'TYPE' => 'STRING',
                'DEFAULT' => '1',
                'REFRESH' => 'Y',
                'COLS' => '2'
            ),
            'POPUP_FORM' => array(
                'PARENT' => 'BASE',
                'NAME' => 'Модальная форма',
                'TYPE' => 'CHECKBOX',
                'REFRESH' => 'Y'
            ),
            'NAME' => array(
                'PARENT' => 'FORM_SUBMIT',
                'NAME' => 'Название формы',
                'TYPE' => 'STRING'
            ),
            'EMAIL_TO' => array(
                'PARENT' => 'FORM_SUBMIT',
                'NAME' => 'E-mail получателя',
                'TYPE' => 'STRING',
                'MULTIPLE' => 'Y'
            ),
            'EVENT_MESSAGE_ID' => Array(
                'PARENT' => 'FORM_SUBMIT',
                'NAME' => 'Почтовые шаблоны для отправки письма',
                'TYPE' => 'LIST',
                'VALUES' => $arEvent,
                'MULTIPLE' => 'Y',
            ),
            'GOALS' => array(
                'PARENT' => 'FORM_SUBMIT',
                'NAME' => 'Цели для Яндекс.Метрика',
                'TYPE' => 'STRING',
                'MULTIPLE' => 'Y'
            ),
            'SUCCESS_MESSAGE_TITLE' => array(
                'PARENT' => 'FORM_SUBMIT',
                'NAME' => 'Заголовок сообщения после успешной отправки',
                'TYPE' => 'STRING',
                'ROWS' => 2
            ),
            'SUCCESS_MESSAGE' => array(
                'PARENT' => 'FORM_SUBMIT',
                'NAME' => 'Сообщение после успешной отправки',
                'TYPE' => 'STRING',
                'ROWS' => 2
            )
        )
    );

    if ($arCurrentValues['POPUP_FORM'] == 'Y') {
        $arComponentParameters['PARAMETERS']['LINK_TO_FORM'] = array(
            'PARENT' => 'BASE',
            'NAME' => 'Текст или html ссылки на модальное окно',
            'TYPE' => 'STRING'
        );
    }
    /** @var int $numFields - количество полей формы */
    $numFields = intval($arCurrentValues['NUM_FIELDS']);
    if ($numFields <= 0) {
        $numFields = 1;
    }

    for ($i = 0; $i < $numFields; $i++) {
        $arComponentParameters['GROUPS']['FIELD_' . $i] = array(
            'SORT' => 400 + $i,
            'NAME' => 'Поле ' . ($i + 1)
        );
        $arComponentParameters['PARAMETERS']['FIELD_' . $i . '_TYPE'] = array(
            'PARENT' => 'FIELD_' . $i,
            'NAME' => 'Тип поля',
            'TYPE' => 'LIST',
            'VALUES' => array(
                'TEXT' => 'Текстовое поле',
                'TEXTAREA' => 'Поле ввода сообщения',
                'LIST' => 'Список',
                'RADIO' => 'Переключатели (RADIO)',
                'CHECKBOX' => 'Флажки (CHECKBOX)',
                'FILE' => 'Файл',
                'HIDDEN' => 'Скрытое поле'
            ),
            'REFRESH' => 'Y'
        );
        $arComponentParameters['PARAMETERS']['FIELD_' . $i . '_TITLE'] = array(
            'PARENT' => 'FIELD_' . $i,
            'NAME' => 'Название поля',
            'TYPE' => 'STRING'
        );
        $arComponentParameters['PARAMETERS']['FIELD_' . $i . '_CODE'] = array(
            'PARENT' => 'FIELD_' . $i,
            'NAME' => 'Код поля (атрибут name)',
            'TYPE' => 'STRING'
        );
        if ($arCurrentValues['FIELD_' . $i . '_TYPE'] != 'HIDDEN') {
            $arComponentParameters['PARAMETERS']['FIELD_' . $i . '_PLACEHOLDER'] = array(
                'PARENT' => 'FIELD_' . $i,
                'NAME' => 'Плейсхолдер или лейбл поля',
                'TYPE' => 'STRING'
            );
        }
        if ($arCurrentValues['FIELD_' . $i . '_TYPE'] == 'TEXT' || empty($arCurrentValues['FIELD_' . $i . '_TYPE'])) {
            $arComponentParameters['PARAMETERS']['FIELD_' . $i . '_MASK'] = array(
                'PARENT' => 'FIELD_' . $i,
                'NAME' => 'Тип текстового поля',
                'TYPE' => 'LIST',
                'VALUES' => array(
                    'SIMPLE' => 'Обычное',
                    'TEXT_RU' => 'Только русские буквы',
                    'PHONE' => 'Телефонный номер',
                    'EMAIL' => 'Электронный адрес',
                    'PHONE_OR_EMAIL' => 'Телефон или электронная почта'
                ),
            );
        }
        if ($arCurrentValues['FIELD_' . $i . '_TYPE'] == 'TEXTAREA') {
            $arComponentParameters['PARAMETERS']['FIELD_' . $i . '_HEIGHT'] = array(
                'PARENT' => 'FIELD_' . $i,
                'NAME' => 'Высота поля для ввода текста',
                'TYPE' => 'LIST',
                'VALUES' => array(
                    'DEFAULT' => '',
                    'TALL' => 'Высокое',
                    'SHORT' => 'Низкое',
                ),
            );
        }
        if ($arCurrentValues['FIELD_' . $i . '_TYPE'] == 'LIST'
            || $arCurrentValues['FIELD_' . $i . '_TYPE'] == 'RADIO'
            || $arCurrentValues['FIELD_' . $i . '_TYPE'] == 'CHECKBOX'
        ) {
            $arComponentParameters['PARAMETERS']['FIELD_' . $i . '_POSSIBLE_VALUES'] = array(
                'PARENT' => 'FIELD_' . $i,
                'NAME' => 'Значения поля',
                'TYPE' => 'STRING',
                'MULTIPLE' => 'Y'
            );
        }
        if ($arCurrentValues['FIELD_' . $i . '_TYPE'] == 'FILE') {
            $arComponentParameters['PARAMETERS']['FIELD_' . $i . '_MULTIPLE'] = array(
                'PARENT' => 'FIELD_' . $i,
                'NAME' => 'Разрешить загружать сразу несколько файлов',
                'TYPE' => 'CHECKBOX'
            );
        }
        if ($arCurrentValues['FIELD_' . $i . '_TYPE'] != 'HIDDEN') {
            $arComponentParameters['PARAMETERS']['FIELD_' . $i . '_REQUIRE'] = array(
                'PARENT' => 'FIELD_' . $i,
                'NAME' => 'Обязательное для заполнения',
                'TYPE' => 'CHECKBOX'
            );

            $arComponentParameters['PARAMETERS']['FIELD_' . $i . '_NOTE'] = array(
                'PARENT' => 'FIELD_' . $i,
                'NAME' => 'Заметка поля',
                'TYPE' => 'STRING'
            );
        }
    }
} catch (Exception $e) {
    ShowError($e->getMessage());
}