<?
/**
 * Created by PhpStorm.
 * User: Kirill Granin
 * Date: 7/5/16
 * Time: 6:45 PM
 * z-labs.ru
 * @ Агенство интернет маркетинга Z-labs - 2016
 */

use \Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

Loader::includeModule("iblock");



$IBLOCK_ID = 4;
$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$IBLOCK_ID));
while ($prop_fields = $properties->GetNext())
{
    //Получаю значения списков
    if($prop_fields["PROPERTY_TYPE"] == "L"){
        $property_enums = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID" => $IBLOCK_ID, "CODE" => $prop_fields["CODE"]));
        while($enum_fields = $property_enums->GetNext())
        {
            $prop_fields["VALUES"][] = $enum_fields;
        }
    }

    $arResult["ORDER_PROPS"][$prop_fields["CODE"]] = $prop_fields;
}
//\ZLabs\ServiceFunction::pre($arResult["ORDER_PROPS"]);
use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();


// process POST data
if (check_bitrix_sessid() && $_REQUEST["iblock_submit"] == "Y")
{
    $arResult["ERRORS"] == array();

    if (count($arResult["ERRORS"]) == 0)
    {
        //echo "<pre>"; print_r($_REQUEST); echo "</pre>";
        //echo "<pre>"; print_r($_FILES); echo "</pre>";
        //die();

        //\ZLabs\ServiceFunction::pre($_POST);
        //\ZLabs\ServiceFunction::pre($_FILES);
        //die();

        //массив для добавления элемента
        $arAddValues = array(
            "IBLOCK_ID" => $IBLOCK_ID,
            "ACTIVE" => "Y",
            "NAME" => "Заявка на присоединение"
        );
        $arAddValues["PROPERTY_VALUES"] = array(
            "USER" => CUser::GetID(),
            "ORDER_STATUS" => 18	, //заявка принята
            "PERSON_TYPE" => $request["PERSON_TYPE"],
            "CONNECT_TYPE" => $request["CONNECT_TYPE"],
            "ORDER_TYPE" => $request["ORDER_TYPE"],
            "RELIABILITY_CATEGORY" => $request["RELIABILITY_CATEGORY"],
            "POWER" => $request["POWER"],
            "OBJECT_ADDRESS" => $request["OBJECT_ADDRESS"],
            "KADASTR_NUMBER" => $request["KADASTR_NUMBER"],
            "DOGOVOR_TYPE" => $request["DOGOVOR_TYPE"],
            //"PRIBOR_UCHETA" => $request["PRIBOR_UCHETA"],
            "COMMENT" => $request["COMMENT"],
            "FIZ_INN" => $request["FIZ_INN"],
            "FIZ_FIO" => $request["FIZ_FIO"],
            "FIZ_EMAIL" => $request["FIZ_EMAIL"],
            "FIZ_PHONE" => $request["FIZ_PHONE"],
            "FIZ_POST_ADDRESS" => $request["FIZ_POST_ADDRESS"],
            "YUR_ORG_NAME" => $request["YUR_ORG_NAME"],
            "YUR_BOSS_FIO" => $request["YUR_BOSS_FIO"],
            "YUR_POST_ADDRESS" => $request["YUR_BOSS_FIO"],
            "YUR_EMAIL" => $request["YUR_EMAIL"],
            "YUR_PHONE" => $request["YUR_PHONE"],
        );



        //Собираю файлы
        $arDel = array(); //массив для удаления временных файлов
        foreach ($_FILES as $file_code => $arFile){
            if($arFile["name"] != ""){
                echo $file_code;
                copy($arFile['tmp_name'],$_SERVER['DOCUMENT_ROOT'].'/upload/file'.$arFile["name"]);
                $arAddValues["PROPERTY_VALUES"][$file_code] = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'].'/upload/file'.$arFile["name"]);
                $arDel[] = $_SERVER['DOCUMENT_ROOT'].'/upload/file'.$arFile["name"];
            }
        }
        //\ZLabs\ServiceFunction::pre($arAddValues["PROPERTY_VALUES"]);


        $oElement = new CIBlockElement();
        $ID_ORDER = $oElement->Add($arUpdateValues, false, false, true);


        if (!$oElement->Add($arAddValues, false, false, true))
        {
            $arResult["ERRORS"][] = $oElement->LAST_ERROR;
            //print_r($arResult["ERRORS"]);
            foreach($arDel as $del){ //удаляю временные файлы
                unlink($del);
            }
        }else{
            foreach($arDel as $del){ //удаляю временные файлы
                unlink($del);
            }
            LocalRedirect("?SUCCES=Y");
        }


    }

}

$this->IncludeComponentTemplate();
