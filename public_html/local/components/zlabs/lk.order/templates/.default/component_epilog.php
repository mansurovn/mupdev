<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
global $APPLICATION;
$APPLICATION->AddHeadScript($templateFolder."/js/jquery.maskedinput.min.js");

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss('/bitrix/templates/agma/css/jquery.kladr.min.css');
Asset::getInstance()->addJs('/bitrix/templates/agma/js/jquery.kladr.min.js');
?>
