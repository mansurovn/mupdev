<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<? //pre($arResult["GRAZHDANSTVO"]);?>
<form class="campaign-order" name="abiturient-order" method="post" enctype="multipart/form-data">
    <section class="campaign-step" style="display: block;" data-step="1">
        <div class="step-title">
            <span class="title-v">Информация об абитуриенте</span>
            <span class="step-info">Шаг 1 из 3</span>
        </div>
        <label for="last_name">Как вас зовут?</label>
        <input type="text" placeholder="Фамилия*" name="ABITURIENT[FAMILIA]" id="last_name" class="req" style="width: 216px;">
        <input type="text" placeholder="Имя*" name="ABITURIENT[IMYA]" class="req" style="width: 216px;">
        <input type="text" placeholder="Отчество" name="ABITURIENT[OTCHESTVO]" style="width: 217px; margin-right: 0px">

        <div class="clear_20"></div>
        <label class="req">Пол*</label>
        <div class="radio-req">
            <input type="radio" name="ABITURIENT[GENDER]" id="gender_m" value="5" checked>
            <label for="gender_m" class="radio">Мужской</label>
            <input type="radio" name="ABITURIENT[GENDER]" id="gender_z" value="6">
            <label for="gender_z" class="radio">Женский</label>
        </div>

        <div class="clear_20"></div>
        <label>Гражданство*</label>
        <select name="ABITURIENT[GRAZHDANSTVO]" class="req">
            <option value="">Выберите из списка</option>
            <? foreach($arResult["GRAZHDANSTVO"] as $arItem):?>
                <option value="<?=$arItem["ID"]?>"><?=$arItem["NAME"]?></option>
            <? endforeach;?>
        </select>

        <div class="clear_20"></div>
        <label for="DOB">Дата и место рождения</label>
        <input type="text" placeholder="Дата рождения*" name="ABITURIENT[DATA_ROZHDENIA]" id="DOB" class="req date-validation">
        <input type="text" placeholder="Место рождения*" name="ABITURIENT[MESTO_ROZHDENIA]" class="req">

        <div class="clear_20"></div>

        <label>Документ, удостоверяющий личность</label>
        <select name="DOKUMENT_UDOST[DOKUMENT_UDOST]">
            <? foreach($arResult["DOKUMENT_UDOST"] as $arItem):?>
                <option value="<?=$arItem["ID"]?>"><?=$arItem["NAME"]?></option>
            <? endforeach;?>
        </select>

        <div class="clear_10"></div>

        <input type="text" name="DOKUMENT_UDOST[SERIA]" placeholder="Серия">
        <input type="text" name="DOKUMENT_UDOST[NOMER]" placeholder="Номер">
        <input type="text" name="DOKUMENT_UDOST[KEM_VYDAN]" placeholder="Кем выдан">
        <input type="text" name="DOKUMENT_UDOST[DATA_VYDACHI]" placeholder="Дата выдачи" class="date-validation">

        <div class="campaign-file-container" style="display: block;">
            <input id="pasport_file" type="file" name="DOKUMENT_UDOST" class="file-req trigger-file">
            <span class="file-trigger" data-file="pasport_file">Прикрепить: копия документа, удостоверяющего личность*</span>
            <div class="file-desc">Размер файла не должен привешать 5Мб</div>
        </div>
        <div class="clear_20"></div>

        <label>Адрес по месту постоянной регистрации</label>
        <div id="ADRES_P">
            <input type="text" name="ADDRESS[ADRES_P_INDEX]" placeholder="Индекс*" class="req" style="width: 125px;">
            <input type="text" name="ADDRESS[ADRES_P_KOD_REGIONA]" placeholder="Код региона" style="width: 125px;">
            <input type="text" name="ADDRESS[ADRES_P_SUBJECT_RF]" placeholder="Субъект РФ*" class="req">
            <div class="clear"></div>
            <input type="text" name="ADDRESS[ADRES_P_NAS_PUNKT]" placeholder="Населенный пункт*" class="req">
            <div class="clear"></div>
            <input type="text" name="ADDRESS[ADRES_P_ULICA]" placeholder="Улица*" class="req">
            <input type="text" name="ADDRESS[ADRES_P_DOM]" placeholder="Дом*" class="req" style="width: 70px;">
            <input type="text" name="ADDRESS[ADRES_P_KORPUS]" placeholder="Корпус" style="width: 100px;">
            <input type="text" name="ADDRESS[ADRES_P_KVARTIRA]" placeholder="Квартира" style="width: 100px;">
        </div>
        <div class="clear_20"></div>
        <label>Адрес по месту проживания</label>
        <div id="ADRES_F">
            <input type="checkbox" name="coincides" id="coincides" value="Y"><label for="coincides" class="checkbox">совпадает с пропиской</label>
            <div class="clear_10"></div>
            <input type="text" name="ADDRESS[ADRES_F_INDEX]" placeholder="Индекс*" class="req" style="width: 125px;">
            <input type="text" name="ADDRESS[ADRES_F_KOD_REGIONA]" placeholder="Код региона" style="width: 125px;">
            <input type="text" name="ADDRESS[ADRES_F_SUBJECT_RF]" placeholder="Субъект РФ*" class="req">
            <div class="clear"></div>
            <input type="text" name="ADDRESS[ADRES_F_NAS_PUNKT]" placeholder="Населенный пункт*" class="req">
            <div class="clear"></div>
            <input type="text" name="ADDRESS[ADRES_F_ULICA]" placeholder="Улица*" class="req">
            <input type="text" name="ADDRESS[ADRES_F_DOM]" placeholder="Дом*" class="req" style="width: 70px;">
            <input type="text" name="ADDRESS[ADRES_F_KORPUS]" placeholder="Корпус" style="width: 100px;">
            <input type="text" name="ADDRESS[ADRES_F_KVARTIRA]" placeholder="Квартира" style="width: 100px;">
        </div>
        <div class="clear_20"></div>

        <label>Контактная информация для связи</label>
        <input type="text" name="CONTACTS[TELEFON]" placeholder="Телефон*" class="req phone-validation" style="width: 216px;">
        <input type="text" name="CONTACTS[DOP_TELEFON]" placeholder="Доп. телефон" style="width: 216px;">
        <input type="text" name="CONTACTS[EMAIL]" placeholder="Эл. почта" style="width: 217px; margin-right: 0px;">
        <div class="clear_20"></div>
    </section>
    <section class="campaign-step" data-step="2">
        <div class="step-title">
            <span class="title-v">Информация об образовании</span>
            <span class="step-info">Шаг 2 из 3</span>
        </div>
        <label>Вид образования</label>
        <select name="VID_OBRAZOVANIA" class="req">
            <option value="">Выберите из списка</option>
            <? foreach($arResult["VID_OBRAZOVANIA"] as $arItem):?>
                <option value="<?=$arItem["ID"]?>"><?=$arItem["NAME"]?></option>
            <? endforeach;?>
        </select>
        <div class="clear_20"></div>
        <label>Документ об образовании</label>
        <select name="DOKUMENT_OB_OBRAZOVANII[TYPE]" class="req">
            <option value="">Выберите из списка</option>
            <? foreach($arResult["DOKUMENT_OB_OBRAZOVANII"] as $arItem):?>
                <option value="<?=$arItem["ID"]?>"><?=$arItem["NAME"]?></option>
            <? endforeach;?>
        </select>
        <div class="clear_10"></div>

        <input type="text" name="DOKUMENT_OB_OBRAZOVANII[NOMER]" placeholder="Номер">
        <input type="text" name="DOKUMENT_OB_OBRAZOVANII[REG_NOMER]" placeholder="Регистрационный номер">
        <input type="text" name="DOKUMENT_OB_OBRAZOVANII[SERIA]" placeholder="Серия">
        <input type="text" name="DOKUMENT_OB_OBRAZOVANII[OKONCHANIE_OBUCHENIA]" placeholder="Год окончания обучения" class="god-validation">
        <input type="text" name="DOKUMENT_OB_OBRAZOVANII[DATA_VYDACHI]" placeholder="Дата выдачи" class="date-validation">
        <input type="text" name="DOKUMENT_OB_OBRAZOVANII[UCHEBNOYE_ZAVEDENIE]" placeholder="Название учебного заведения">
        <input type="text" name="DOKUMENT_OB_OBRAZOVANII[KEM_VYDAN]" placeholder="Кем выдан">
        <div class="campaign-file-container" style="display: block;">
            <input id="obrazovanie_file" type="file" name="DOKUMENT_OB_OBRAZOVANII_FILE" class="file-req trigger-file">
            <span class="file-trigger" data-file="obrazovanie_file">Прикрепить: копия документа об образовании*</span>
            <div class="file-desc">Размер файла не должен привешать 5Мб</div>
        </div>

        <div class="clear_20"></div>

        <label>Результаты тестирования</label>
        <? /*<div class="desc-text">Отметьте специальности для поступления начиная с самого высокого приоритета.</div>*/?>
        <? foreach($arResult["TEST_RESULTS"] as $arItem):?>
            <div class="test-results-item">
                <select name="TEST_RESULTS[TYPE][]" class="req" style="width: 80px;">
                    <option value="ЕГЭ">ЕГЭ</option>
                    <option value="ВИ">Вступительные испытания</option>
                </select>
                <select name="TEST_RESULTS[PREDMET][]" class="req" style="width: 170px">
                    <option value="">Выберите предмет</option>
                    <? foreach($arResult["TEST_RESULTS"] as $arItem2):?>
                        <option value="<?=$arItem2["ID"]?>"><?=$arItem2["NAME"]?></option>
                    <? endforeach;?>
                </select>
                <input type="text" name="TEST_RESULTS[NOMER_SERTIFIKATA][]" placeholder="Номер сертификата" style="width: 175px;" class="ege-req">
                <input type="text" name="TEST_RESULTS[BALL][]" placeholder="Балл" style="width: 68px;" class="ege-req">
                <input type="text" name="TEST_RESULTS[YEAR][]" placeholder="Год сдачи" style="width: 105px;" class="ege-req">
            </div>
        <? endforeach;?>
        <div class="exams-passport">
            <div class="clear_20"></div>
            <label>Серия и номер паспорта по которым сдавалось испытание</label>
            <input type="text" name="TEST_RESULTS[SERIA_PASPORTA]" placeholder="Серия*" class="req">
            <input type="text" name="TEST_RESULTS[NOMER_PASPORTA]" placeholder="Номер*" class="req">
        </div>
        <? /*<div class="campaign-node-add-container"><span id="test_result_add" class="campaign-node-add">Добавить результат</span></div>*/?>
        <? /*
        <div class="error-block">
            <b>Ваш результат по дисциплине «Русский язык» меньше проходного.</b><br>
            Дальнейшее заполнение анкеты невозможно. С информацией по встпительным баллам вы можете ознакомиться на
            странице
        </div>
        */?>
        <div class="clear_20"></div>
        <label>Иностранные языки</label>
        <? foreach($arResult["INOSTRANNYE_YAZYKI"] as $arItem):?>
            <div>
                <input type="checkbox" name="INOSTRANNYE_YAZYKI[]" id="lang-<?=$arItem["ID"]?>" value="<?=$arItem["ID"]?>"><label for="lang-<?=$arItem["ID"]?>" class="checkbox"><?=$arItem["NAME"]?></label>
            </div>
            <div class="yazyk-old">
                <input type="text" name="INOSTRANNYE_YAZYKI_TIME[<?=$arItem["ID"]?>]" id="lang-time-<?=$arItem["ID"]?>" value="" placeholder="Продолжительность изучения">
            </div>
        <? endforeach;?>

        <div class="clear_20"></div>
        <label>Индивидуальные достижения</label>
        <div class="desc-text">Выберите одно или несколько индивидуальных достижений, для подтверждения прикрепите отсканированную версию документа, подтверждающего ваше достижение.</div>
        <div class="inostrannye-yazyki">
            <? foreach($arResult["INDIVIDUALNYE_DOSTIZHENIA"] as $arItem):?>
                <div class="list-item">
                    <input type="checkbox" name="INDIVIDUALNYE_DOSTIZHENIA[<?=$arItem["ID"]?>]" id="dost-<?=$arItem["ID"]?>" value="<?=$arItem["ID"]?>" class="check-file-req"><label for="dost-<?=$arItem["ID"]?>" class="checkbox"><?=$arItem["NAME"]?></label><br>
                    <div class="campaign-file-container">
                        <? foreach($arResult["PODTVERZHDAUSHIE_DOCUMENTY"][$GLOBALS["IBLOCK_KLASSIFIKATOR"]["ИндивидуальныеДостижения"]][$arItem["XML_ID"]] as $arDocs):?>
                            <div class="campaign-file-item">
                                <input type="file" name="INDIVIDUALNYE_DOSTIZHENIA_FILE[<?=$arItem["ID"]?>][]" class="trigger-file" data-file="dost-<?=$arItem["ID"]?>">
                                <span class="file-trigger" data-file="dost-<?=$arItem["ID"]?>">Прикрепить: <?=$arDocs["NAME"]?></span>
                                <div class="file-desc">Размер файла не должен привешать 5Мб</div>
                            </div>
                        <? endforeach;?>
                    </div>
                </div>
            <? endforeach;?>
        </div>
    </section>
    <section class="campaign-step" data-step="3">
        <div class="step-title">
            <span class="title-v">Заявление абитуриента</span>
            <span class="step-info">Шаг 3 из 3</span>
        </div>
        <label>Специальность</label>
        <div class="desc-text">Со вступительными испытаниями для каждой специальности вы можете ознакомиться в <a href="#" title="Подробнее" target="_blank">документе</a>. Для подачи заявления вам необходимо выбрать специальность в порядке приоритета от высшего к
            низшему.
        </div>
        <? $SPECIALNOSTI_COUNT = count($arResult["SPECIALNOSTI"]);?>
        <? for($i = 0; $i < $SPECIALNOSTI_COUNT+1; $i++):?>
            <div>
                <select name="SPECIALNOST[SPEC][]" class="<? if($i == 0):?>req<? endif;?>">
                    <option value="">Выберите специальность</option>
                    <? foreach($arResult["SPECIALNOSTI"] as $arItem):?>
                        <option value="<?=$arItem["ID"]?>"><?=$arItem["NAME"]?></option>
                    <? endforeach;?>
                </select>
                <select name="SPECIALNOST[OSNOVANIE][]" style="width: 300px;" class="specialnost-osnovanie <? if($i == 0):?>req<? endif;?>">
                    <option value="">Выберите основание для поступления</option>
                    <? foreach($arResult["OSNOVANIE"] as $arItem):?>
                        <option value="<?=$arItem["ID"]?>"><?=$arItem["NAME"]?></option>
                    <? endforeach;?>
                </select>
                <div class="celevoe">
                    <input name="SPECIALNOST[CELEVOE][]" type="text" placeholder="Название направляющей организации" value="" style="width: 350px;" class="<? if($i == 0):?>req<? endif;?>">
                    <div class="campaign-file-container">
                        <input type="file" name="SPECIALNOST[CELEVOE_FILE][]" class="req">
                        <span class="file-trigger" data-input-name="red_diploma_file">Прикрепить: договор о целевом обучении</span>
                        <div class="file-desc">Размер файла не должен привешать 5Мб</div>
                    </div>

                </div>
            </div>
        <? endfor;?>
        <? /*
        <div class="clearfix"></div>
        <span class="speciality-delete">Удалить</span>
        <div class="clearfix"></div>
        <div class="campaign-node-add-container">
            <span id="speciality_add" class="campaign-node-add">Добавить специальность</span></div>
        */?>
        <div class="clear_20"></div>

        <label>Форма обучения</label>
        <input type="radio" name="mode_of_study" id="mos-1" value="1" checked>
        <label for="mos-1" class="radio">Очная</label>

        <div class="clear_20"></div>

        <label>Льготы</label>
        <div class="desc-text">Выберите одну или несколько льгот, под которые вы попадаете, для подтверждения прикрепите отсканированную версию документа, подтверждающего вашу льготу.</div>
        <div class="inostrannye-yazyki">
            <? foreach($arResult["LGOTY"] as $arItem):?>
                <div class="list-item">
                    <input type="checkbox" name="LGOTY[]" id="lgota-<?=$arItem["ID"]?>" value="<?=$arItem["ID"]?>" class="check-file-req"><label for="lgota-<?=$arItem["ID"]?>" class="checkbox"><?=$arItem["NAME"]?></label><br>
                    <div class="campaign-file-container">
                        <?
                        /*
                        <input type="file" name="LGOTY_FILE[]" class="trigger-file" data-file="lgota-<?=$arItem["ID"]?>">
                        <span class="file-trigger" data-file="lgota-<?=$arItem["ID"]?>">Прикрепить: <?=$arResult["PODTVERZHDAUSHIE_DOCUMENTY"][$GLOBALS["IBLOCK_KLASSIFIKATOR"]["Льготы"]][$arItem["XML_ID"]]["NAME"]?></span>
                        <div class="file-desc">Размер файла не должен привешать 5Мб</div>
                        */
                        ?>
                        <? foreach($arResult["PODTVERZHDAUSHIE_DOCUMENTY"][$GLOBALS["IBLOCK_KLASSIFIKATOR"]["Льготы"]][$arItem["XML_ID"]] as $arDocs):?>
                            <div class="campaign-file-item">
                                <input type="file" name="LGOTY_FILE[<?=$arItem["ID"]?>][]" class="trigger-file" data-file="lgota-<?=$arItem["ID"]?>">
                                <span class="file-trigger" data-file="lgota-<?=$arItem["ID"]?>">Прикрепить: <?=$arDocs["NAME"]?></span>
                                <div class="file-desc">Размер файла не должен привешать 5Мб</div>
                            </div>
                        <? endforeach;?>
                    </div>
                </div>
            <? endforeach;?>
        </div>

        <div class="clear_20"></div>

        <label>Специальные отметки</label>
        <div class="desc-text">Выберите одну или несколько отметок, под которые вы попадаете, для подтверждения прикрепите отсканированную версию документа, подтверждающего вашу отметку.</div>
        <div class="inostrannye-yazyki">
            <? foreach($arResult["MARKS"] as $arItem):?>
                <div class="list-item">
                    <input type="checkbox" name="MARK[]" id="mark-<?=$arItem["ID"]?>" value="<?=$arItem["ID"]?>" class="check-file-req"><label for="mark-<?=$arItem["ID"]?>" class="checkbox"><?=$arItem["NAME"]?></label><br>
                    <div class="campaign-file-container">
                        <? /*
                        <input type="file" name="MARK_FILE[]" class="trigger-file" data-file="mark-<?=$arItem["ID"]?>">
                        <span class="file-trigger" data-file="mark-<?=$arItem["ID"]?>">Прикрепить: <?=$arResult["PODTVERZHDAUSHIE_DOCUMENTY"][$GLOBALS["IBLOCK_KLASSIFIKATOR"]["ОсобыеОтметки"]][$arItem["XML_ID"]]["NAME"]?></span>
                        <div class="file-desc">Размер файла не должен привешать 5Мб</div>
                        */ ?>
                        <? foreach($arResult["PODTVERZHDAUSHIE_DOCUMENTY"][$GLOBALS["IBLOCK_KLASSIFIKATOR"]["ОсобыеОтметки"]][$arItem["XML_ID"]] as $arDocs):?>
                            <div class="campaign-file-item">
                                <input type="file" name="MARK_FILE[<?=$arItem["ID"]?>][]" class="trigger-file" data-file="mark-<?=$arItem["ID"]?>">
                                <span class="file-trigger" data-file="mark-<?=$arItem["ID"]?>">Прикрепить: <?=$arDocs["NAME"]?></span>
                                <div class="file-desc">Размер файла не должен привешать 5Мб</div>
                            </div>
                        <? endforeach;?>
                    </div>
                </div>
            <? endforeach;?>
        </div>

        <div class="clear_20"></div>

        <label>В общежитии нуждаюсь</label>
        <input type="radio" name="OBSHEZHITIE" id="d-1" value="7">
        <label for="d-1" class="radio">Да</label>
        <input type="radio" name="OBSHEZHITIE" id="d-2" value="8" checked>
        <label for="d-2" class="radio">Нет</label>

        <div class="clear_20"></div>
        <div>
            <label class="req">Отношения к лицам прибывшим из Крыма*</label>
            <input type="radio" name="KRYM" id="krym_yes" value="9">
            <label for="krym_yes" class="radio">Да</label>
            <input type="radio" name="KRYM" id="krym_no" value="10" checked>
            <label for="krym_no" class="radio">Нет</label>
            <div class="campaign-file-container">
                <input type="file" name="KRYM_FILE" id="krym_file">
                <span class="file-trigger" data-input-name="krym_file">Прикрепить: копия паспорта гражданина РФ, выданный не ранее 18 марта 2014 г. (включая данные о регистрации)</span>
                <div class="file-desc">Размер файла не должен привешать 5Мб</div>
            </div>
        </div>
        <div class="clear_20"></div>

        <label>Создать для меня особые условия на экзамене</label>
        <input type="radio" name="USLOVIA" id="p-0" value="11">
        <label for="p-0" class="radio">Да</label>
        <input type="radio" name="USLOVIA" id="p-1" value="12" checked>
        <label for="p-1" class="radio">Нет</label>

        <div class="clear_20"></div>
        <label>Согласие на обработку персональных данных</label>
        <div class="error-block" style="margin-top: 5px; padding: 20px;">
            <div>
                Чтобы заполнить данное поле вам необходимо скачать <a href="/abiturientu-priyemnaya-komissiya/zayavlenie-abiturienta/" title="Скачать заявление">заявление на обработку персональных данных</a> и отсканированную копию прикрепить в поле ниже.
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="campaign-file-container" style="display: block;">
            <input id="soglasie_pers" type="file" name="SOGLASIE_PERS" class="trigger-file">
            <span class="file-trigger" data-file="soglasie_pers">Прикрепить: согласие на обработку персональных данных</span>
            <div class="file-desc">Размер файла не должен привешать 5Мб</div>
        </div>
    </section>
    <div class="campaign-step-footer">
        <div class="campaign-step-footer-check">
            <input type="checkbox" name="acquainted" id="acquainted" class="req">
            <label for="acquainted" class="checkbox">С <a href="#" title="Посмотреть правила подачи апелляции" target="_blank">правилами</a> подачи апелляции ознакомлен</label>
        </div>
        <div class="campaign-step-footer-buttons">
            <a id="prev_button" class="campaign-button disabled" href="#" title="Назад">Назад</a>&nbsp;&nbsp;
            <a id="next_button" class="campaign-button disabled" href="#" title="Далее">Далее</a>
        </div>
        <div class="clear"></div>
    </div>
    <input name="iblock_submit" type="hidden" value="Y">
    <?=bitrix_sessid_post()?>
</form>
