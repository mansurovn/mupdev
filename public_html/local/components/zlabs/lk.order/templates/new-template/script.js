function ValidationAbiturient(){
    var var_return = true;
    
    var str = $(".date-validation:visible");
    var dt = new Date()
    str_val = str.val();

    /*if (!/^\d\d\.\d\d\.\d{4}$/.test (str_val)) {alert ('Error, unformat'); return}
    var a0 = function (x) {return ((x < 10) ? '0' : '') + x},
    t = str_val.split ('.'), ndt = new Date (+t [2], t[1] - 1, +t [0]);
    with (ndt) var tst = [a0 (getDate ()), a0 (getMonth () + 1), getFullYear ()].join ('.');*/

    /*if (tst != str_val || t[2]>dt.getFullYear()+10 || t[2]<dt.getFullYear()-30) {
        var_return = false;
        str.addClass('novalid');
    }*/

    $(".campaign-step:visible input[type=text].req:visible").each(function(){
        if($(this).val() == ""){
            $(this).addClass("novalid");
            var_return = false;
        }
    })
    $(".campaign-step:visible select.req").each(function(){
        //alert(123);
        if($(this).val() == ""){
            $(this).addClass("novalid");
            var_return = false;
        }
    })

    $(".campaign-step:visible input[type=checkbox].check-file-req:checked").each(function(){
        var cur_id = $(this).attr("id");
        if($("input[data-file=" + cur_id + "]").val() == ""){
            var_return = false;
            $(".file-trigger[data-file=" + cur_id + "]").addClass("novalidtext");
        }
    })

    $(".campaign-step:visible input[type=file].file-req").each(function(){
        var cur_id = $(this).attr("id");
        if($("input#"+cur_id).val() == ""){
            var_return = false;
            $(".file-trigger[data-file=" + cur_id + "]").addClass("novalidtext");
        }
    })

    /*var var_radio = false;
    $(".campaign-step:visible .radio-req input").each(function(){
        if($(this).is(":checked")){
            var_radio = true;
            continue;
        }
    })
    if(var_radio == false){

    }*/

    return var_return;
}


$(document).ready(function(){
    $(".date-validation").mask("99.99.9999");

    var DATE = $('.campaign-order').find('.date-validation');
    if (DATE.length > 0)DATE.pickmeup({
        format: 'd.m.Y',
        hide_on_select: true,
        locale: {
            days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятницы', 'Суббота', 'Воскресенье'],
            daysShort: ['Вск', 'Пон', 'Вто', 'Сре', 'Чет', 'Пят', 'Суб', 'Вос'],
            daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
        }
    });

    $(".god-validation").mask("9999");
    $(".phone-validation").mask("+7 (999) 999-99-99");

    $("#coincides").change(function(){
        if($(this).is(":checked")){
            $('#ADRES_P input[type=text]').each(function(){
                cur_val = $(this).val();
                cur_name = $(this).attr("name").replace("_P_","_F_");

                $('input[name="' + cur_name + '"]').val(cur_val).removeClass("novalid");
            })
        }
    });

    $(".inostrannye-yazyki input[type=checkbox]").change(function(){
        if($(this).is(":checked")){
            $(this).parent().children(".campaign-file-container").show();
        }else{
            $(this).parent().children(".campaign-file-container").hide();
        }
    });

    $(".specialnost-osnovanie").change(function(){
        if($(this).find("option:selected").text() == "Целевой прием"){
            $(this).parent().children(".celevoe").show();
        }else{
            $(this).parent().children(".celevoe").hide();
        }
    });

    $("input[name=KRYM]").change(function(){
        if($(this).val() == 9){
            $(this).parent().children(".campaign-file-container").show();
        }else{
            $(this).parent().children(".campaign-file-container").hide();
        }
    });

    $("#acquainted").change(function(){
        if($(this).is(":checked")){
            $("#next_button").removeClass("disabled");
        }else{
            $("#next_button").addClass("disabled");
        }
    })

    $("#next_button").click(function(){
        if(!$(this).hasClass("disabled")){
            if(ValidationAbiturient()){
                if($(".campaign-step:visible").data("step") == 3){
                    $("#prev_button").addClass("disabled");
                    $("#next_button").addClass("disabled").html("Загрузка...");
                    $("form[name=abiturient-order]").submit();

                    /*setTimeout(function(){
                        $(".campaign-order").html("<div style='font-size: 15px; font-weight: bold;'>Ваше заявление принято!</div>");
                        $('html, body').stop().animate({
                            scrollTop: $(".personal-section").offset().top
                        }, 1000);
                    }, 4000)*/
                }else{
                    $("#prev_button").removeClass("disabled");
                    $(".campaign-step:visible").hide().next(".campaign-step").show();

                    if($(".campaign-step:visible").data("step") == 3){
                        $("#next_button").html("Сохранить");
                    }
                    $('html, body').stop().animate({
                        scrollTop: $(".campaign-order").offset().top
                    }, 1000);
                }
            }else{
                if($(".campaign-order:visible .novalid").size() > 0){
                    $('html, body').stop().animate({scrollTop: $(".campaign-order:visible .novalid:first").offset().top -  100}, 1000);
                }else{
                    $('html, body').stop().animate({scrollTop: $(".campaign-order:visible .novalidtext:first").offset().top -  100}, 1000);
                }

            }
        }

        return false;
    })

    $("#prev_button").click(function(){
        if(!$(this).hasClass("disabled")){
            if($(".campaign-step:visible").data("step") == 2) $("#prev_button").addClass("disabled");

            $(".campaign-step:visible").hide().prev(".campaign-step").show();
            $('html, body').stop().animate({
                scrollTop: $(".campaign-order").offset().top
            }, 1000);

            if($(".campaign-step:visible").data("step") != 3){
                $("#next_button").html("Далее");
            }
        }

        return false;
    })

    $(".campaign-order input[type=text]").focus(function(){
        $(this).removeClass("novalid");
    })

    $(".campaign-order select").click(function(){
        $(this).removeClass("novalid");
    })

    $(".file-trigger").click(function(){
        $(this).prev("input[type=file]").trigger('click');
    })

    //Прикрепление файла
    $("input.trigger-file").change(function(){
        var cur_file_name = $(this).val();
        cur_file_name = cur_file_name.split("\\");
        cur_file_name = cur_file_name[cur_file_name.length-1];
        $(this).next(".file-trigger").html("Заменить: " + cur_file_name).removeClass("novalidtext");

    })

    //Языки
    $(".campaign-order input[name=\"INOSTRANNYE_YAZYKI[]\"]").change(function(){
        if($(this).is(":checked")){
            $(this).parent().next(".yazyk-old").show();
        }else{
            $(this).parent().next(".yazyk-old").hide();
        }
    })
    //Результаты тестирования
    $(".campaign-order select[name=\"TEST_RESULTS[TYPE][]\"]").change(function(){
        if($(this).val() == "ВИ"){
            $(this).parent().find(".ege-req").hide();
        }
        if($(this).val() == "ЕГЭ"){
            $(this).parent().find(".ege-req").show();
        }
        var all_vi = true; //проверяю, если все экзамены ВИ, то скрываю паспортные данные
        $(".campaign-order select[name=\"TEST_RESULTS[TYPE][]\"]").each(function(){
            if($(this).val() == "ЕГЭ"){
                all_vi = false;
            }
        })
        if(all_vi){
            $(".exams-passport").hide();
        }else{
            $(".exams-passport").show();
        }
    });
    /*$("#prev_button.disabled").on("click", function(){
        return false;
    })*/
})