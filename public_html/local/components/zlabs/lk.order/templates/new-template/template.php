<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<? //pre($arResult["GRAZHDANSTVO"]);?>

<div class="application visibility">
    <? if ($_REQUEST["SUCCES"] == "Y") : ?>
        Ваша заявка успешно принята!
    <? else : ?>
    <div class="application__tab-list-container">
        <ul class="application__tab-list clearfix" role="tablist">
            <li class="application__tab application__tab_active" data-form="1">
                <div class="link application-tab__link">Выбор типа заявки</div>
            </li>
            <li class="application__tab" data-form="2">
                <div class="link application-tab__link">Сведения об объекте</div>
            </li>
            <li class="application__tab" data-form="3">
                <div class="link application-tab__link">Прикрепление документов</div>
            </li>
            <li class="application__tab" data-form="4">
                <div class="link application-tab__link">Контактные данные</div>
            </li>
        </ul>
    </div>
    <form action="" class="feedback-form application__form" method="post" enctype="multipart/form-data" name="ORDER_FORM">
            <div class="application__tabpanels">
                <div class="application__tabpanel application__tabpanel_active application__tabpanel_in clearfix"
                     id="form1">
                    <div class="feedback-form__group application__form-group" id="person">
                        <span class="feedback-form__label">Заявитель</span>
                        <span class="pop-up__trigger">
                            ?
                            <span class="pop-up pop-up_grey pop-up_above pop-up__hint">
                                Лицо, от имени которого подается заявка
                            </span>
                        </span>
                        <input type="hidden" name="PERSON_TYPE"
                               class="feedback-form__control feedback-form__control_type_list
                               feedback-form__control_type_list_hidden"
                               title="Выберите тип заявителя" value="">
                        <div class="feedback-form__control_type_list_pseudo">
                            <span class="feedback-form__control_type_current-option">
                                Выберите тип заявителя
                            </span>
                            <span class="feedback-form__icon icon icon_arrow-angle-down"></span>
                        </div>
                        <div class="feedback-form__control_type_options nano">
                            <ul class="feedback-form__options-container nano-content">
                                <? foreach ($arResult["ORDER_PROPS"]["PERSON_TYPE"]["VALUES"] as $val) :?>
                                    <li data-value="<?= $val["ID"] ?>"
                                        class="feedback-form__control_type_option"><?= $val["VALUE"] ?></li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="feedback-form__group application__form-group">
                        <span class="feedback-form__label">Выберите тип присоединения</span>
                        <span class="pop-up__trigger">?<span
                                    class="pop-up pop-up_grey pop-up_above pop-up__hint">
                                К технологическому присоединению относятся случаи присоединения впервые
                                вводимых в эксплуатацию, ранее присоединенных реконструируемых
                                энергопринимающих устройств, максимальная мощность которых увеличивается, а также
                                на случаи, при которых в отношении ранее присоединенных энергопринимающих
                                устройств изменяются категория надежности электроснабжения, точки присоединения,
                                виды производственной деятельности, не влекущие пересмотр величины максимальной
                                мощности, но изменяющие схему внешнего электроснабжения таких энергопринимающих
                                устройств
                            </span>
                        </span>
                        <input type="hidden" name="CONNECT_TYPE"
                               class="feedback-form__control feedback-form__control_type_list
                               feedback-form__control_type_list_hidden"
                               title="Выберите тип присоединения" value="">
                        <div class="feedback-form__control_type_list_pseudo">
                            <span class="feedback-form__control_type_current-option">
                                Выберите тип присоединения
                            </span>
                            <span class="feedback-form__icon icon icon_arrow-angle-down"></span>
                        </div>
                        <div class="feedback-form__control_type_options nano">
                            <ul class="feedback-form__options-container nano-content">
                                <? foreach ($arResult["ORDER_PROPS"]["CONNECT_TYPE"]["VALUES"] as $val) :?>
                                    <li data-value="<?= $val["ID"] ?>"
                                        class="feedback-form__control_type_option"><?= $val["VALUE"] ?></li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="feedback-form__group application__form-group">
                        <span class="feedback-form__label">Выберите тип заявки</span>
                        <span class="pop-up__trigger">
                            ?
                            <span class="pop-up pop-up_grey pop-up_above pop-up__hint">
                                Из выпадающего списка выберите тип заявки.
                                Более подробную информацию вы можете получить по
                                <a href="#" title="Подробнее">ссылке</a>
                            </span>
                        </span>
                        <input type="hidden" name="ORDER_TYPE"
                               class="feedback-form__control feedback-form__control_type_list
                               feedback-form__control_type_list_hidden"
                               title="Выберите тип заявки" value="">
                        <div class="feedback-form__control_type_list_pseudo">
                            <span class="feedback-form__control_type_current-option">Выберите тип заявки</span>
                            <span class="feedback-form__icon icon icon_arrow-angle-down"></span>
                        </div>
                        <div class="feedback-form__control_type_options nano">
                            <ul class="feedback-form__options-container nano-content">
                                <? foreach ($arResult["ORDER_PROPS"]["ORDER_TYPE"]["VALUES"] as $val) :?>
                                    <li data-value="<?= $val["ID"] ?>"
                                        class="feedback-form__control_type_option"><?= $val["VALUE"] ?></li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="feedback-form__group application__form-group">
                        <a href="#calc" class="link application__calc-link"
                           title="Калькулятор расчета стоимости">
                            <span class="icon icon_calculator application__icon"></span>
                            <span class="application__calc-underline">Калькулятор расчета стоимости</span>
                        </a>
                    </div>
                    <div class="feedback-form__group application__form-group application__form-group_big-offset">
                        <h4 class="application-form__header">Категория надёжности и мощность</h4>
                        <span class="feedback-form__label feedback-form__label_wo-offset">
                            Выберите категорию надёжности и мощность присоединяемого устройства
                        </span>
                        <span class="pop-up__trigger">
                            ?
                            <span class="pop-up pop-up_grey pop-up_above pop-up__hint">
                                Из выпадающего списка выберите тип заявки.
                                Более подробную информацию вы можете получить по
                                <a href="#" title="Подробнее">ссылке</a>
                            </span>
                        </span>
                    </div>
                    <div class="clearfix">
                        <div class="feedback-form__group application__form-group application__form-group_small">
                            <div class="feedback-form__label">Категория надежности</div>
                            <input type="hidden" name="RELIABILITY_CATEGORY"
                                   class="feedback-form__control feedback-form__control_type_list
                                   feedback-form__control_type_list_hidden"
                                   title="Категория надежности" value="">
                            <div class="feedback-form__control_type_list_pseudo" id="reliability">
                                <span class="feedback-form__control_type_current-option">
                                    Выберите категорию надежности
                                </span>
                                <span class="feedback-form__icon icon icon_arrow-angle-down"></span>
                            </div>
                            <div class="feedback-form__control_type_options nano">
                                <ul class="feedback-form__options-container nano-content">
                                    <li class="feedback-form__control_type_option" data-value="2">
                                        2
                                    </li>
                                    <li class="feedback-form__control_type_option" data-value="3">
                                        3
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="feedback-form__group application__form-group application__form-group_small">
                            <label for="power" class="feedback-form__label">Максимальная мощность (всего), кВт</label>
                            <input type="text" name="POWER" id="power"
                                   class="feedback-form__control feedback-form__control_type_text">
                        </div>
                    </div>
                    <div class="feedback-form__group application__form-group
                    application__form-group_big-offset clearfix"
                         role="tablist">
                        <a href="#form2" class="btn application-form__btn application-form__btn_next">
                            <span class="btn__link application-form__btn-link">
                                <span class="application-form__icon icon icon_arrow-right_after">
                                    Следующий шаг
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="application__tabpanel " id="form2">

                    <div class="feedback-form__group application__form-group application__form-group_big-offset">
                        <h4 class="application-form__header">Укажите данные по объекту</h4>
                        <label for="address" class="feedback-form__label">Адрес присоединяемого
                            объекта</label>
                        <span class="pop-up__trigger">?<span
                                    class="pop-up pop-up_grey pop-up_above pop-up__hint">
                                Адрес указывается в соответствии с документом, подтверждающим право собственности
или иное предусмотренное законом основание на объект капитального строительства
(нежилое помещение в таком объекте капитального строительства) и (или) земельный
участок, на котором расположены (будут располагаться) объекты заявителя, либо право
собственности или иное предусмотренное законом основание на энергопринимающие
устройства (например, свидетельство о государственной регистрации права, выписка из
единого государственного реестра прав на недвижимое имущество, договор аренды,
договор субаренды)
                            </span>
                        </span>
                        <input type="text" name="OBJECT_ADDRESS"
                               class="feedback-form__control feedback-form__control_type_text"
                               value="" placeholder="" id="address">
                    </div>
                    <div class="feedback-form__group application__form-group">
                        <label class="feedback-form__label">Кадастровый номер</label>
                        <span class="pop-up__trigger">?
                            <span class="pop-up pop-up_grey pop-up_above pop-up__hint">
                                Указывается в соответствии с документом, подтверждающим право собственности
                            </span>
                        </span>
                        <input type="text" name="KADASTR_NUMBER"
                               class="feedback-form__control feedback-form__control_type_text"
                               value="" placeholder="">
                    </div>

                    <div class="feedback-form__group application__form-group">
                        <label for="add" class="feedback-form__label">Прочая информация (желаете сообщить
                            дополнительно)</label>
                        <textarea
                                class="feedback-form__control feedback-form__control_type_textarea application__textarea"
                                name="COMMENT"
                                id="add"></textarea>
                    </div>
                    <div class="feedback-form__group application__form-group application__form-group_big-offset clearfix">
                        <span class="feedback-form__label feedback-form__label_small">Вид договора, обеспечивающий продажу электроэнергии</span>
                        <div class="feedback-form__radio-list">
                            <? foreach ($arResult["ORDER_PROPS"]["DOGOVOR_TYPE"]["VALUES"] as $i => $val): ?>
                                <div class="feedback-form__radio-item">
                                    <input type="radio" id="radio2_item<?= $i ?>" name="DOGOVOR_TYPE" value="<?=$val["ID"]?>"
                                           <? if ($i == 0): ?>checked<? endif; ?>
                                           class="feedback-form__control feedback-form__control_type_radio">

                                    <label for="radio2_item<?= $i ?>" class="feedback-form__radio-pseudo-item">
                                        <span class="feedback-form__radio-marker"></span>
                                        <?= $val["VALUE"] ?></label>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                    <div class="feedback-form__group application__form-group application__form-group_big-offset clearfix"
                         role="tablist">
                        <a href="#form1" class="btn application-form__btn application-form__btn_prev">
                                    <span class="btn__link application-form__btn-link"><span
                                                class="application-form__icon icon icon_arrow-left">Назад</span></span>
                        </a>
                        <a href="#form3" class="btn application-form__btn application-form__btn_next">
                                    <span class="btn__link application-form__btn-link"><span
                                                class="application-form__icon icon icon_arrow-right_after">Следующий шаг</span></span>
                        </a>
                    </div>
                </div>
                <div class="application__tabpanel  application__counter" id="form3">
                    <div class="feedback-form__group application__form-group">
                        <h4 class="application-form__header">Важно</h4>
                        <div class="application__alert">Нижеследующие формы принимают файлы документов в форматах jpg,
                            pdf, zip, rar.
                            Документы одного типа отсканируйте и прикрепите одним файлом в архиве.
                        </div>
                    </div>
                    <div class="feedback-form__fiz">
                        <div class="feedback-form__group application__form-group application__counter-item">
                            <div class="spoiler">
                                <div class="spoiler__item icon icon_arrow-angle-down_after spoiler-item__icon">
                                    <div class="spoiler-item__title spoiler__trigger">Копия документа, подтверждающего
                                        право собственности
                                        <span class="pop-up__trigger">?<span
                                                    class="pop-up pop-up_grey pop-up_above pop-up__hint">Копия документа, подтверждающего право собственности или иное предусмотренное законом основание на объект капитального строительства (нежилое помещение в таком объекте капитального строительства) и (или) земельный участок, на котором расположены (будут располагаться) объекты заявителя, либо право собственности или иное предусмотренное законом основание на энергопринимающие устройства (например, свидетельство о государственной регистрации права, выписка из единого государственного реестра прав на недвижимое имущество, договор аренды, договор субаренды)</span></span>
                                    </div>
                                    <div class="spoiler__hide-block">
                                        <input class="feedback-form__control feedback-form__control_type_file"
                                               type="file" name="FIZ_FILE_PRAVOSOBSTVENNOSTI" autocomplete="off">
                                        <a href="#"
                                           class="link feedback-form__pseudo-file-control icon icon_upload application__pseudo-file"><span
                                                    class="application__pseudo-file-link">Прикрепить файл</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="feedback-form__group application__form-group application__counter-item">
                            <div class="spoiler">
                                <div class="spoiler__item icon icon_arrow-angle-down_after spoiler-item__icon">
                                    <div class="spoiler-item__title spoiler__trigger">Копия паспорта <span
                                                class="pop-up__trigger">?<span
                                                    class="pop-up pop-up_grey pop-up_above pop-up__hint">Разворот с местом выдачи, персональными данными и фотографией владельца паспорта; разворот с отметками о регистрации</span></span>
                                    </div>
                                    <div class="spoiler__hide-block">
                                        <input class="feedback-form__control feedback-form__control_type_file"
                                               type="file" name="FIZ_FILE_PASPORT_1" autocomplete="off">
                                        <a href="#"
                                           class="link feedback-form__pseudo-file-control icon icon_upload application__pseudo-file"><span
                                                    class="application__pseudo-file-link">Прикрепить: разворот с личными данными</span></a>
                                        <input class="feedback-form__control feedback-form__control_type_file"
                                               type="file" name="FIZ_FILE_PASPORT_2" autocomplete="off">
                                        <a href="#"
                                           class="link feedback-form__pseudo-file-control icon icon_upload application__pseudo-file"><span
                                                    class="application__pseudo-file-link">Прикрепить: разворот с регистрацией</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="feedback-form__group application__form-group application__counter-item">
                            <div class="spoiler">
                                <div class="spoiler__item icon icon_arrow-angle-down_after spoiler-item__icon">
                                    <div class="spoiler-item__title spoiler__trigger">Копия доверенности или иных
                                        документов
                                        <span class="pop-up__trigger">?<span
                                                    class="pop-up pop-up_grey pop-up_above pop-up__hint">Заполняется в случае если заявка подается в сетевую организацию представителем заявителя. Копия доверенности или иных документов, подтверждающих полномочия представителя заявителя, подающего и получающего документы.</span></span>
                                    </div>
                                    <div class="spoiler__hide-block">
                                        <input class="feedback-form__control feedback-form__control_type_file"
                                               type="file" name="FIZ_FILE_DOVERENNOST" autocomplete="off">
                                        <a href="#"
                                           class="link feedback-form__pseudo-file-control icon icon_upload application__pseudo-file"><span
                                                    class="application__pseudo-file-link">Прикрепить файл</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="feedback-form__yur">
                        <div class="feedback-form__group application__form-group">
                            <div class="feedback-form__group application__form-group">
                                <h4 class="application-form__header">Укажите данные организации</h4>
                                <label for="yur_org_name" class="feedback-form__label">Название организации</label>
                                <input type="text" name="YUR_ORG_NAME" id="yur_org_name"
                                       class="feedback-form__control feedback-form__control_type_text" value=""
                                       placeholder="">
                            </div>
                        </div>
                        <div class="feedback-form__group application__form-group">
                            <label for="yur_head" class="feedback-form__label">ФИО руководителя</label>
                            <input type="text" name="YUR_BOSS_FIO" id="yur_head"
                                   class="feedback-form__control feedback-form__control_type_text" value=""
                                   placeholder="">
                        </div>
                        <div class="feedback-form__group application__form-group application__counter-item">
                            <div class="spoiler">
                                <div class="spoiler__item icon icon_arrow-angle-down_after spoiler-item__icon">
                                    <div class="spoiler-item__title spoiler__trigger">Карточка компании</div>
                                    <div class="spoiler__hide-block">
                                        <input class="feedback-form__control feedback-form__control_type_file"
                                               type="file" name="YUR_FILE_COMPANY_CARD" autocomplete="off">
                                        <a href="#"
                                           class="link feedback-form__pseudo-file-control icon icon_upload application__pseudo-file"><span
                                                    class="application__pseudo-file-link">Прикрепить файл</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="feedback-form__group application__form-group application__counter-item">
                            <div class="spoiler">
                                <div class="spoiler__item icon icon_arrow-angle-down_after spoiler-item__icon">
                                    <div class="spoiler-item__title spoiler__trigger">Банковские реквизиты</div>
                                    <div class="spoiler__hide-block">
                                        <input class="feedback-form__control feedback-form__control_type_file"
                                               type="file" name="YUR_FILE_BANK_CARD" autocomplete="off">
                                        <a href="#"
                                           class="link feedback-form__pseudo-file-control icon icon_upload application__pseudo-file"><span
                                                    class="application__pseudo-file-link">Прикрепить файл</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="feedback-form__group application__form-group application__counter-item">
                            <div class="spoiler">
                                <div class="spoiler__item icon icon_arrow-angle-down_after spoiler-item__icon">
                                    <div class="spoiler-item__title spoiler__trigger">Копия документа, подтверждающего
                                        право собственности
                                        <span class="pop-up__trigger">?<span
                                                    class="pop-up pop-up_grey pop-up_above pop-up__hint">Заполняется в случае если заявка подается в сетевую организацию представителем заявителя. Копия доверенности или иных документов, подтверждающих полномочия представителя заявителя, подающего и получающего документы.</span></span>
                                    </div>
                                    <div class="spoiler__hide-block">
                                        <input class="feedback-form__control feedback-form__control_type_file"
                                               type="file" name="YUR_FILE_PRAVOSOBSTVENNOSTI" autocomplete="off">
                                        <a href="#"
                                           class="link feedback-form__pseudo-file-control icon icon_upload application__pseudo-file"><span
                                                    class="application__pseudo-file-link">Прикрепить файл</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="feedback-form__group application__form-group application__counter-item">
                            <div class="spoiler">
                                <div class="spoiler__item icon icon_arrow-angle-down_after spoiler-item__icon">
                                    <div class="spoiler-item__title spoiler__trigger">Копия доверенности или иных
                                        документов
                                        <span class="pop-up__trigger">?<span
                                                    class="pop-up pop-up_grey pop-up_above pop-up__hint">Копия документа, подтверждающего право собственности или иное предусмотренное законом основание на объект капитального строительства (нежилое помещение в таком объекте капитального строительства) и (или) земельный участок, на котором расположены (будут располагаться) объекты заявителя, либо право собственности или иное предусмотренное законом основание на энергопринимающие устройства (например, свидетельство о государственной регистрации права, выписка из единого государственного реестра прав на недвижимое имущество, договор аренды, договор субаренды)</span></span>
                                    </div>
                                    <div class="spoiler__hide-block">
                                        <input class="feedback-form__control feedback-form__control_type_file"
                                               type="file" name="YUR_FILE_DOVERENNOST" autocomplete="off">
                                        <a href="#"
                                           class="link feedback-form__pseudo-file-control icon icon_upload application__pseudo-file"><span
                                                    class="application__pseudo-file-link">Прикрепить файл</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="feedback-form__group application__form-group application__form-group_big-offset clearfix"
                         role="tablist">
                        <a href="#form2" class="btn application-form__btn application-form__btn_prev">
                                    <span class="btn__link application-form__btn-link"><span
                                                class="application-form__icon icon icon_arrow-left">Назад</span></span>
                        </a>
                        <a href="#form4" class="btn application-form__btn application-form__btn_next">
                                    <span class="btn__link application-form__btn-link"><span
                                                class="application-form__icon icon icon_arrow-right_after">Следующий шаг</span></span>
                        </a>
                    </div>
                </div>
                <div class="application__tabpanel  application__counter" id="form4">
                    <div class="feedback-form__yur feedback-form__yur-contact">
                        <div class="feedback-form__group application__form-group">
                            <div class="feedback-form__group application__form-group">
                                <h4 class="application-form__header">Ваши контактные данные</h4>
                                <label for="yur_post" class="feedback-form__label">Почтовый адрес</label>
                                <input type="text" name="YUR_POST_ADDRESS" id="yur_post"
                                       class="feedback-form__control feedback-form__control_type_text" value=""
                                       placeholder="">
                            </div>
                        </div>
                        <div class="feedback-form__group application__form-group">
                            <label for="yur_email" class="feedback-form__label">Эл. почта
                                <span class="pop-up__trigger">?<span
                                            class="pop-up pop-up_grey pop-up_above pop-up__hint">На данную почту будут высылаться уведомления об изменении статуса вашего обращения.</span></span>
                            </label>
                            <input type="text" name="YUR_EMAIL" id="yur_email"
                                   class="feedback-form__control feedback-form__control_type_text" value=""
                                   placeholder="">
                        </div>
                        <div class="feedback-form__group application__form-group">
                            <label for="yur_tel" class="feedback-form__label">Телефон
                                <span class="pop-up__trigger">?<span
                                            class="pop-up pop-up_grey pop-up_above pop-up__hint">Телефон контактного лица для уточнения деталей по обращению.</span></span>
                            </label>
                            <input type="text" name="YUR_PHONE" id="yur_tel"
                                   class="feedback-form__control feedback-form__control_type_text" value=""
                                   placeholder="">
                        </div>
                    </div>
                    <div class="feedback-form__fiz feedback-form__fiz-contact">
                        <div class="feedback-form__group application__form-group">
                            <div class="feedback-form__group application__form-group">
                                <h4 class="application-form__header">Ваши контактные данные</h4>
                                <label for="fiz_post" class="feedback-form__label">Почтовый адрес</label>
                                <input type="text" name="FIZ_POST_ADDRESS" id="fiz_post"
                                       class="feedback-form__control feedback-form__control_type_text" value=""
                                       placeholder="">
                            </div>
                        </div>
                        <div class="feedback-form__group application__form-group">
                            <label for="fiz_email" class="feedback-form__label">Эл. почта
                                <span class="pop-up__trigger">?<span
                                            class="pop-up pop-up_grey pop-up_above pop-up__hint">На данную почту будут высылаться уведомления об изменении статуса вашего обращения.</span></span>
                            </label>
                            <input type="text" name="FIZ_EMAIL" id="fiz_email"
                                   class="feedback-form__control feedback-form__control_type_text" value=""
                                   placeholder="">
                        </div>
                        <div class="feedback-form__group application__form-group">
                            <label for="fiz_tel" class="feedback-form__label">Телефон
                                <span class="pop-up__trigger">?<span
                                            class="pop-up pop-up_grey pop-up_above pop-up__hint">Телефон контактного лица для уточнения деталей по обращению.</span></span>
                            </label>
                            <input type="text" name="FIZ_PHONE" id="fiz_tel"
                                   class="feedback-form__control feedback-form__control_type_text" value=""
                                   placeholder="">
                        </div>
                    </div>
                    <div class="feedback-form__user-consent application__form-group_big-offset">
                        <label class="custom-control custom-control_type_checkbox main-user-consent-request">
                            <input class="custom-control__input" type="checkbox" value="Y" checked="" name="">
                            <span class="custom-control__indicator icon icon_checked"></span>
                            <span class="custom-control__name">Я согласен на обработку </span><a href="#"
                                                                                                 target="_blank"
                                                                                                 class="link custom-control__link">персональных
                                данных</a></label>
                    </div>
                    <div class="feedback-form__group application__form-group clearfix" role="tablist">
                        <a href="#form3" class="btn application-form__btn application-form__btn_prev" title="Назад">
                                    <span class="btn__link application-form__btn-link"><span
                                                class="application-form__icon icon icon_arrow-left">Назад</span></span>
                        </a>
                        <div class="feedback-form__submit-container application-form__submit-container">
                            <button class="btn btn_main feedback-form__submit application-form__submit" type="submit" name="submit"><span
                                        class="btn__link">Отправить заявку</span></button>
                        </div>
                    </div>
                </div>
            </div>
            <input name="iblock_submit" type="hidden" value="Y">
            <?= bitrix_sessid_post() ?>
        </form>
</div>


<? endif; ?>

