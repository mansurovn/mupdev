<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arComponentDescription = array(
    "NAME" => GetMessage("CAMPAIGN_ORDER_NAME"),
    "DESCRIPTION" => GetMessage("CAMPAIGN_ORDER_DESCRIPTION"),
    "PATH" => array(
        "ID" => "zlabs",
        "NAME" => "Z-Labs"
    ),
);